﻿using CarRent.Enums;
using CarRent.Modeli;
using System;
using System.Windows.Forms;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace CarRent
{
    public partial class frmKorisnik : Form
    {
        private Korisnik korisnik;
        private FormMode formMode;
        private List<Korisnik> korisnici;

        public frmKorisnik(Korisnik korisnik, FormMode formMode, List<Korisnik> korisnici)
        {
            InitializeComponent();
            this.korisnik = korisnik;
            this.formMode = formMode;
            this.korisnici = korisnici;
        }

        private void frmKorisnik_Load(object sender, EventArgs e)
        {
            // Osnovni podaci
            txIme.Text = korisnik.Ime;
            txPrezime.Text = korisnik.Prezime;
            txJmbg.Text = korisnik.Jmbg;
            dtDatumRodjenja.Value = korisnik.DatumRodjenja;
            dtDatumRodjenja.MaxDate = DateTime.Now.Date;
            txTelefon.Text = korisnik.Telefon;

            // Podaci za prijavu
            txKorisnickoIme.Text = korisnik.KorisnickoIme;
            txLozinka.Text = korisnik.Lozinka;
            txLozinka2.Text = korisnik.Lozinka;

            // Tip korisnika
            rbAdmin.Checked = korisnik.Tip == TipKorisnika.Administrator;
            rbKupac.Checked = korisnik.Tip == TipKorisnika.Kupac;
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            // var mainForm = (frmMain)this.MdiParent;
            // var korisnici = mainForm.Korisnici;

            if (!ProveraUnosa())
            {
                this.DialogResult = DialogResult.None;
                return;
            }

            korisnik.Ime = txIme.Text;
            korisnik.Prezime = txPrezime.Text;
            korisnik.Jmbg = txJmbg.Text;
            korisnik.DatumRodjenja = dtDatumRodjenja.Value.Date;
            korisnik.Telefon = txTelefon.Text;
            korisnik.KorisnickoIme = txKorisnickoIme.Text;
            korisnik.Lozinka = txLozinka.Text;

            if (formMode == FormMode.Dodaj)
            {
                var sledeciId = (korisnici.Count > 0 ?  korisnici.Max(c => c.Id) : 0) + 1;
                korisnik.Id = sledeciId;

                korisnici.Add(korisnik);
                FileHelper.SnimiUDatoteku("korisnici.xml", korisnici);
            }
            else
            {
                for (int i = 0; i < korisnici.Count; i++)
                {
                    if (korisnik.Id == korisnici[i].Id)
                    {
                        korisnici[i] = korisnik;
                        FileHelper.SnimiUDatoteku("korisnici.xml", korisnici);
                        break;
                    }
                }
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
            return;
        }

        private bool ProveraUnosa()
        {
            if (string.IsNullOrEmpty(txIme.Text))
            {
                MessageBox.Show("Molimo unesite ime!");
                txIme.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txPrezime.Text))
            {
                MessageBox.Show("Molimo unesite prezime!");
                txPrezime.Focus();
                return false;
            }

            if (dtDatumRodjenja.Value > DateTime.Now.Date)
            {
                MessageBox.Show("Datum rođеnja ne može biti u budućnosti!");
                dtDatumRodjenja.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txJmbg.Text))
            {
                MessageBox.Show("Molimo unesite JMBG!");
                txJmbg.Focus();
                return false;
            }

            var isJmbg = Regex.IsMatch(txJmbg.Text, @"\d{13}");
            if (!isJmbg)
            {
                MessageBox.Show("JMBG nije ispravan! Mora sadrzati 13 cifara");
                txJmbg.Focus();
                return false;
            }

            if (!txJmbg.Text.Substring(0, 7).Equals(dtDatumRodjenja.Value.ToString("ddMM") + dtDatumRodjenja.Value.ToString("yyyy").Substring(1)))
            {
                MessageBox.Show("JMBG se ne poklapa sa datumom rođеnja.");
                txJmbg.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txTelefon.Text))
            {
                MessageBox.Show("Molimo unesite Telefon!\nTelefon mora biti u ormatu: XXX/000-00-00");
                txTelefon.Focus();
                return false;
            }

            var isPhoneNumber = Regex.IsMatch(txTelefon.Text, @"\d{3}\/-?\d{3}-\d{2}-\d{2}");
            if (!isPhoneNumber)
            {
                MessageBox.Show("Nije unet regularan telefonski broj!\nMolimo unsite broj telefona u formatu: XXX/000-00-00");
                txTelefon.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txKorisnickoIme.Text))
            {
                MessageBox.Show("Molimo unesite korisničko ime!");
                txKorisnickoIme.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txLozinka.Text))
            {
                MessageBox.Show("Molimo unesite lozinku!");
                txLozinka.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txLozinka2.Text))
            {
                MessageBox.Show("Molimo unesite ponovljenu lozinku!");
                txLozinka2.Focus();
                return false;
            }

            if (!txLozinka.Text.Equals(txLozinka2.Text))
            {
                MessageBox.Show("Lozinka i ponovljena lozinka se ne poklapaju!");
                txLozinka2.Focus();
                return false;
            }

            return true;
        }

        public void DisableTip()
        {
            rbAdmin.Enabled = false;
            rbKupac.Enabled = false;
        }

        private void cmdOtkazi_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
