﻿namespace CarRent
{
    partial class frmPregled<T>
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbNaslov = new System.Windows.Forms.Label();
            this.lbLista = new System.Windows.Forms.ListBox();
            this.cmdDodaj = new System.Windows.Forms.Button();
            this.cmdPromeni = new System.Windows.Forms.Button();
            this.cmdBriši = new System.Windows.Forms.Button();
            this.cmdIzlaz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbNaslov
            // 
            this.lbNaslov.AutoSize = true;
            this.lbNaslov.Location = new System.Drawing.Point(12, 20);
            this.lbNaslov.Name = "lbNaslov";
            this.lbNaslov.Size = new System.Drawing.Size(35, 13);
            this.lbNaslov.TabIndex = 0;
            this.lbNaslov.Text = "label1";
            // 
            // lbLista
            // 
            this.lbLista.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbLista.FormattingEnabled = true;
            this.lbLista.HorizontalScrollbar = true;
            this.lbLista.Location = new System.Drawing.Point(15, 51);
            this.lbLista.Name = "lbLista";
            this.lbLista.Size = new System.Drawing.Size(692, 381);
            this.lbLista.TabIndex = 1;
            this.lbLista.SelectedIndexChanged += new System.EventHandler(this.lbLista_SelectedIndexChanged);
            this.lbLista.DoubleClick += new System.EventHandler(this.lbLista_DoubleClick);
            // 
            // cmdDodaj
            // 
            this.cmdDodaj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdDodaj.Location = new System.Drawing.Point(713, 51);
            this.cmdDodaj.Name = "cmdDodaj";
            this.cmdDodaj.Size = new System.Drawing.Size(75, 23);
            this.cmdDodaj.TabIndex = 2;
            this.cmdDodaj.Text = "&Dodaj";
            this.cmdDodaj.UseVisualStyleBackColor = true;
            this.cmdDodaj.Click += new System.EventHandler(this.cmdDodaj_Click);
            // 
            // cmdPromeni
            // 
            this.cmdPromeni.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdPromeni.Location = new System.Drawing.Point(713, 80);
            this.cmdPromeni.Name = "cmdPromeni";
            this.cmdPromeni.Size = new System.Drawing.Size(75, 23);
            this.cmdPromeni.TabIndex = 3;
            this.cmdPromeni.Text = "&Promeni";
            this.cmdPromeni.UseVisualStyleBackColor = true;
            this.cmdPromeni.Click += new System.EventHandler(this.cmdPromeni_Click);
            // 
            // cmdBriši
            // 
            this.cmdBriši.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdBriši.Location = new System.Drawing.Point(713, 109);
            this.cmdBriši.Name = "cmdBriši";
            this.cmdBriši.Size = new System.Drawing.Size(75, 23);
            this.cmdBriši.TabIndex = 4;
            this.cmdBriši.Text = "Iz&briši";
            this.cmdBriši.UseVisualStyleBackColor = true;
            this.cmdBriši.Click += new System.EventHandler(this.cmdBriši_Click);
            // 
            // cmdIzlaz
            // 
            this.cmdIzlaz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdIzlaz.Location = new System.Drawing.Point(713, 159);
            this.cmdIzlaz.Name = "cmdIzlaz";
            this.cmdIzlaz.Size = new System.Drawing.Size(75, 23);
            this.cmdIzlaz.TabIndex = 5;
            this.cmdIzlaz.Text = "I&zlaz";
            this.cmdIzlaz.UseVisualStyleBackColor = true;
            this.cmdIzlaz.Click += new System.EventHandler(this.cmdIzlaz_Click);
            // 
            // frmPregled
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cmdIzlaz);
            this.Controls.Add(this.cmdBriši);
            this.Controls.Add(this.cmdPromeni);
            this.Controls.Add(this.cmdDodaj);
            this.Controls.Add(this.lbLista);
            this.Controls.Add(this.lbNaslov);
            this.Name = "frmPregled";
            this.Text = "frmPregled";
            this.Load += new System.EventHandler(this.frmPregled_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label lbNaslov;
        private System.Windows.Forms.ListBox lbLista;
        private System.Windows.Forms.Button cmdDodaj;
        private System.Windows.Forms.Button cmdPromeni;
        private System.Windows.Forms.Button cmdBriši;
        private System.Windows.Forms.Button cmdIzlaz;
    }
}