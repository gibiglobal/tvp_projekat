﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace CarRent
{
    static class FileHelper
    {
        public static bool SnimiUDatoteku<T>(string fileName, T objekat)
        {
            try
            {
                var fs = new FileStream(fileName, FileMode.Create);
                var xmlSerializer = new XmlSerializer(typeof(T));          

                xmlSerializer.Serialize(fs, objekat);

                fs.Flush();
                fs.Close();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static T ProcitajIzDatoteke<T>(string fileName)
        {
            FileInfo fi = new FileInfo(fileName);
            if (!fi.Exists)
            {
                return default(T);
            }

            try
            {
                var fs = new FileStream(fileName, FileMode.Open);
                var xmlSerializer = new XmlSerializer(typeof(T));

                T objekat = (T)xmlSerializer.Deserialize(fs);
                
                fs.Close();

                return objekat;
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }
    }
}
