﻿using CarRent.Modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRent.Helpers
{
    static class AutomobilHelper
    {
        #region Marke

        public static List<MarkaAuto> GetSveMarke()
        {
            var lista = new List<MarkaAuto>
            {
                GetAlfaRomeo(),
                GetAudi(),
                GetBmw(),
                GetCitroen(),
                GetFiat(),
                GetFord(),
                GetKia(),
                GetMercedesBenz(),
                GetPeugeot(),
                GetRenault(),
                GetSkoda(),
                GetZastava()
            };

            return lista;
        }

        public static MarkaAuto GetAlfaRomeo()
        {
            var alfa = new MarkaAuto
            {
                Marka = "Alfa Romeo",
                Modeli = new List<string>
                {
                    "33",
                    "75",
                    "145",
                    "146",
                    "147",
                    "155",
                    "156",
                    "156 Crosswagon",
                    "159",
                    "164",
                    "166",
                    "Brera",
                    "Giulia",
                    "Giulietta",
                    "GT",
                    "GTV",
                    "MiTo",
                    "Spider",
                    "Stelvio",
                    "Ostalo"
                }
            };

            return alfa;
        }

        public static MarkaAuto GetAudi()
        {
            var audi = new MarkaAuto
            {
                Marka = "Audi",
                Modeli = new List<string>
                {
                    "80",
                    "90",
                    "100",
                    "200",
                    "A1",
                    "A2",
                    "A3",
                    "A4",
                    "A4 Allroad",
                    "A5",
                    "A6",
                    "A6 Allroad",
                    "A7",
                    "A8",
                    "Coupe",
                    "Q2",
                    "Q3",
                    "Q5",
                    "Q7",
                    "Q8",
                    "Quattro",
                    "R8",
                    "RS6",
                    "S3",
                    "S4",
                    "S5",
                    "S8",
                    "SQ5",
                    "SQ7",
                    "TT",
                    "TTS",
                    "Ostalo"
                }
            };

            return audi;
        }

        public static MarkaAuto GetBmw()
        {
            var bmw = new MarkaAuto
            {
                Marka = "BMW",
                Modeli = new List<string>
                {
                    "114",
                    "116",
                    "118",
                    "120",
                    "123",
                    "125",
                    "135",
                    "216",
                    "218",
                    "220",
                    "315",
                    "316",
                    "318",
                    "320",
                    "323",
                    "324",
                    "325",
                    "328",
                    "330",
                    "335",
                    "Compact",
                    "318 GT",
                    "320 GT",
                    "418",
                    "420",
                    "428",
                    "430",
                    "435",
                    "518",
                    "520",
                    "523",
                    "524",
                    "528",
                    "530",
                    "535",
                    "540",
                    "545",
                    "M550",
                    "520 GT",
                    "530 GT",
                    "630",
                    "640",
                    "645",
                    "650",
                    "725",
                    "728",
                    "730",
                    "735",
                    "740",
                    "745",
                    "750",
                    "760",
                    "M2",
                    "M3",
                    "M4",
                    "M5",
                    "M6",
                    "X1",
                    "X2",
                    "X3",
                    "X4",
                    "X5",
                    "X6",
                    "X7",
                    "i3",
                    "Z3",
                    "Z4",
                    "2002",
                    "Ostalo"
                }
            };

            return bmw;
        }

        public static MarkaAuto GetCitroen()
        {
            var citroen = new MarkaAuto
            {
                Marka = "Citroen",
                Modeli = new List<string>
                {
                    "2CV",
                    "Ami",
                    "AX",
                    "Berlingo",
                    "BX",
                    "C-Crosser",
                    "C-ELYSEE",
                    "C1",
                    "C2",
                    "C3",
                    "C3 Aircross",
                    "C3 Picasso",
                    "C3 pluriel",
                    "C4",
                    "C4 Aircross",
                    "C4 Cactus",
                    "C4 Grand Picasso",
                    "C4 Picasso",
                    "C5",
                    "C6",
                    "C8",
                    "C15",
                    "CX",
                    "DS",
                    "DS3",
                    "DS4",
                    "DS5",
                    "Dyane",
                    "GS",
                    "Jumpy",
                    "Nemo",
                    "Saxo",
                    "Visa",
                    "Xantia",
                    "XM",
                    "Xsara",
                    "Xsara Picasso",
                    "ZX",
                    "Ostalo"
                }
            };

            return citroen;
        }

        public static MarkaAuto GetFiat()
        {
            var fiat = new MarkaAuto
            {
                Marka = "Fiat",
                Modeli = new List<string>
                {
                    "124",
                    "125",
                    "126",
                    "127",
                    "131",
                    "500",
                    "500C",
                    "500L",
                    "500X",
                    "600",
                    "850",
                    "1100",
                    "1107",
                    "1300",
                    "Albea",
                    "Barchetta",
                    "Brava",
                    "Bravo",
                    "Campagnola",
                    "Cinquecento",
                    "Coupe",
                    "Croma",
                    "Doblo",
                    "EVO",
                    "Fiorino",
                    "Freemont",
                    "Grande Punto",
                    "Idea",
                    "Linea",
                    "Marea",
                    "Marengo",
                    "Multipla",
                    "Palio",
                    "Panda",
                    "Punto",
                    "Qubo",
                    "Scudo",
                    "Sedici",
                    "Seicento",
                    "Spider Europa",
                    "Stilo",
                    "Tempra",
                    "Tipo",
                    "Ulysse",
                    "Uno",
                    "x 1/9",
                    "Ostalo"
                }
            };

            return fiat;
        }

        public static MarkaAuto GetFord()
        {
            var ford = new MarkaAuto
            {
                Marka = "Ford",
                Modeli = new List<string>
                {
                    "Aerostar",
                    "B-Max",
                    "C-Max",
                    "Capri",
                    "Cougar",
                    "Courier",
                    "Eco Sport",
                    "Edge",
                    "Escort",
                    "Excursion",
                    "Expedition",
                    "Explorer",
                    "Fiesta",
                    "Focus",
                    "Fusion",
                    "Galaxy",
                    "Granada",
                    "Grand C-Max",
                    "Ka",
                    "Kuga",
                    "Maverick",
                    "Mondeo",
                    "Mustang",
                    "Orion",
                    "Probe",
                    "Puma",
                    "Ranger",
                    "S-Max",
                    "Scorpio",
                    "Sierra",
                    "Street Ka",
                    "Taunus",
                    "Taurus",
                    "Tourneo",
                    "Tourneo Connect",
                    "Tourneo Courier",
                    "Ostalo"
                }
            };

            return ford;
        }

        public static MarkaAuto GetKia()
        {
            var kia = new MarkaAuto
            {
                Marka = "Kia",
                Modeli = new List<string>
                {
                    "Carens",
                    "Carnival",
                    "cee`d",
                    "cee`d sw",
                    "Cerato",
                    "Clarus",
                    "Joice",
                    "Magentis",
                    "Optima",
                    "Picanto",
                    "Pride",
                    "pro_cee`d",
                    "Rio",
                    "Sephia",
                    "Shuma",
                    "Sorento",
                    "Soul",
                    "Spectra",
                    "Sportage",
                    "Stonic",
                    "Venga",
                    "Ostalo"
                }
            };

            return kia;
        }

        public static MarkaAuto GetMercedesBenz()
        {
            var mercedes = new MarkaAuto
            {
                Marka = "Mercedes Benz",
                Modeli = new List<string>
                {
                    "A 140",
                    "A 150",
                    "A 160",
                    "A 170",
                    "A 180",
                    "A 190",
                    "A 200",
                    "A 210",
                    "A 220",
                    "A 250",
                    "A 45 AMG",
                    "B 150",
                    "B 160",
                    "B 170",
                    "B 180",
                    "B 200",
                    "C 180",
                    "C 200",
                    "C 220",
                    "C 230",
                    "C 240",
                    "C 250",
                    "C 270",
                    "C 300",
                    "C 320",
                    "C 32 AMG",
                    "C 63 AMG",
                    "CE 200",
                    "CE 220",
                    "CE 230",
                    "Citan",
                    "CLA 180",
                    "CLA 200",
                    "CLA 220",
                    "CL 500",
                    "CL 63 AMG",
                    "CLC 220",
                    "CLK 200",
                    "CLK 220",
                    "CLK 230",
                    "CLK 240",
                    "CLK 270",
                    "CLK 320",
                    "CLS 220",
                    "CLS 250",
                    "CLS 300",
                    "CLS 320",
                    "CLS 350",
                    "CLS 400",
                    "CLS 500",
                    "CLS 55 AMG",
                    "CLS 63 AMG",
                    "E 200",
                    "E 220",
                    "E 230",
                    "E 240",
                    "E 250",
                    "E 260",
                    "E 270",
                    "E 280",
                    "E 290",
                    "E 300",
                    "E 320",
                    "E 350",
                    "E 400",
                    "E 43 AMG",
                    "E 500",
                    "G 230",
                    "G 250",
                    "G 270",
                    "G 290",
                    "G 300",
                    "G 350",
                    "G 400",
                    "G 500",
                    "G 63 AMG",
                    "GL 320",
                    "GL 350",
                    "GL 420",
                    "GL 450",
                    "GL 500",
                    "GLA 45 AMG",
                    "GLA 180",
                    "GLA 200",
                    "GLA 220",
                    "GLC 220",
                    "GLC 250",
                    "GLC 350",
                    "GLE 250",
                    "GLE 300",
                    "GLE 350",
                    "GLK 200",
                    "GLK 220",
                    "GLK 250",
                    "GLK 320",
                    "GLK 350",
                    "GLS 350 D",
                    "MB 100",
                    "ML 230",
                    "ML 250",
                    "ML 270",
                    "ML 280",
                    "ML 320",
                    "ML 350",
                    "ML 400",
                    "ML 430",
                    "ML 500",
                    "ML 55 AMG",
                    "ML 63 AMG",
                    "R 280",
                    "R 320",
                    "R 350",
                    "S 220",
                    "S 250",
                    "S 280",
                    "S 300",
                    "S 320",
                    "S 350",
                    "S 400",
                    "S 420",
                    "S 500",
                    "S 550",
                    "S 600",
                    "S 63 AMG",
                    "SL 280",
                    "SL 350",
                    "SL 380",
                    "SL 500",
                    "SLK 200",
                    "SLK 230",
                    "Vaneo",
                    "190",
                    "Ostalo"
                }
            };

            return mercedes;
        }

        public static MarkaAuto GetPeugeot()
        {
            var peugeot = new MarkaAuto
            {
                Marka = "Peugeot",
                Modeli = new List<string>
                {
                    "106",
                    "107",
                    "108",
                    "204",
                    "205",
                    "206",
                    "207",
                    "208",
                    "301",
                    "305",
                    "306",
                    "307",
                    "308",
                    "309",
                    "404",
                    "405",
                    "406",
                    "407",
                    "504",
                    "505",
                    "508",
                    "604",
                    "605",
                    "607",
                    "806",
                    "807",
                    "1007",
                    "2008",
                    "3008",
                    "4007",
                    "4008",
                    "5008",
                    "Bipper",
                    "Expert",
                    "Partner",
                    "Ranch",
                    "RCZ",
                    "Rifter",
                    "TePee",
                    "Ostalo"
                }
            };

            return peugeot;
        }

        public static MarkaAuto GetRenault()
        {
            var renault = new MarkaAuto
            {
                Marka = "Renault",
                Modeli = new List<string>
                {
                    "Avantime",
                    "Captur",
                    "Clio",
                    "Espace",
                    "Express",
                    "Fluence",
                    "Grand Espace",
                    "Grand Modus",
                    "Grand Scenic",
                    "Kadjar",
                    "Kangoo",
                    "Koleos",
                    "Laguna",
                    "Latitude",
                    "Megane",
                    "Modus",
                    "R 4",
                    "R 5",
                    "R 9",
                    "R 10",
                    "R 11",
                    "R 12",
                    "R 14",
                    "R 18",
                    "R 19",
                    "R 20",
                    "R 21",
                    "R 25",
                    "Rapid",
                    "RX",
                    "Safrane",
                    "Scenic",
                    "Talisman",
                    "Thalia",
                    "Twingo",
                    "Vel Satis",
                    "Wind",
                    "Ostalo"
                }
            };

            return renault;
        }

        public static MarkaAuto GetSkoda()
        {
            var skoda = new MarkaAuto
            {
                Marka = "Škoda",
                Modeli = new List<string>
                {
                    "100",
                    "105",
                    "120",
                    "1000 MB",
                    "Citigo",
                    "Fabia",
                    "Favorit",
                    "Felicia",
                    "Karoq",
                    "Kodiaq",
                    "Octavia",
                    "Praktik",
                    "Rapid",
                    "Roomster",
                    "Superb",
                    "Yeti",
                    "Ostalo"
                }
            };

            return skoda;
        }

        public static MarkaAuto GetZastava()
        {
            var zastava = new MarkaAuto
            {
                Marka = "Zastava",
                Modeli = new List<string>
                {
                    "10",
                    "101",
                    "128",
                    "1300",
                    "1500",
                    "750",
                    "850",
                    "AR 55",
                    "Florida",
                    "Florida In",
                    "Koral",
                    "Koral In",
                    "Poly",
                    "Skala 55",
                    "Yugo 45",
                    "Yugo 55",
                    "Yugo 60",
                    "Yugo 65",
                    "Yugo Cabrio",
                    "Yugo Ciao",
                    "Yugo In L",
                    "Yugo Tempo",
                    "Ostalo"
                }
            };

            return zastava;
        }

        #endregion

        #region Godiste

        public static List<string> GetGodista()
        {
            var godista = new List<string>();

            for (int i = 1930; i < DateTime.Now.Year; i++)
            {
                godista.Add(i.ToString());
            }

            return godista;
        }

        #endregion

        #region Kubikaza

        public static List<string> GetKubikaze()
        {
            return new List<string>
            {
                "500 cm3",
                "1150 cm3",
                "1300 cm3",
                "1600 cm3",
                "1800 cm3",
                "2000 cm3",
                "2500 cm3",
                "3000 cm3",
                "3500 cm3",
                "4500 cm3"
            };
        }

        #endregion

        #region Pogon

        public static List<string> GetPogoni()
        {
            return new List<string>
            {
                "Prednji",
                "Zadnji",
                "4x4",
                "4x4 reduktor"
            };
        }

        #endregion

        #region Menjaci

        public static List<string> GetMenjaci()
        {
            return new List<string>
            {
                "Manuelni 4 brzine",
                "Manuelni 5 brzine",
                "Manuelni 6 brzine",
                "Poluautomatski",
                "Automatski"
            };
        }

        #endregion

        #region Karoserije

        public static List<string> GetKaroserije()
        {
            return new List<string>
            {
                "Liminzina",
                "Hečbek",
                "Karavan",
                "Kupe",
                "Kabriolet/Roadster",
                "Monovolumen (Mini Van)",
                "SUV",
                "Pickup"
            };
        }

        #endregion

        #region Goriva

        public static List<string> GetGoriva()
        {
            return new List<string>
            {
                "Dizel",
                "Benzin + Gas (TNG)",
                "Metan CNG",
                "Električni pogon",
                "Hibridni pogon"
            };
        }

        #endregion

        #region Vrata

        public static List<string> GetBrVrata()
        {
            var vrata = new List<string>();

            for (int i = 1; i < 8; i++)
            {
                vrata.Add(i.ToString());
            }

            return vrata;
        }

        #endregion
    }
}
