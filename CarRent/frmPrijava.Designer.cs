﻿namespace CarRent
{
    partial class frmPrijava
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txKorisnickoIme = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txLozinka = new System.Windows.Forms.TextBox();
            this.cmdPrijava = new System.Windows.Forms.Button();
            this.cmdIzlaz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(167, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Molimo unesite podatke za prijavu";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Korisničko ime:";
            // 
            // txKorisnickoIme
            // 
            this.txKorisnickoIme.Location = new System.Drawing.Point(96, 46);
            this.txKorisnickoIme.Name = "txKorisnickoIme";
            this.txKorisnickoIme.Size = new System.Drawing.Size(124, 20);
            this.txKorisnickoIme.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Lozinka";
            // 
            // txLozinka
            // 
            this.txLozinka.Location = new System.Drawing.Point(96, 72);
            this.txLozinka.Name = "txLozinka";
            this.txLozinka.PasswordChar = '*';
            this.txLozinka.Size = new System.Drawing.Size(124, 20);
            this.txLozinka.TabIndex = 4;
            // 
            // cmdPrijava
            // 
            this.cmdPrijava.Location = new System.Drawing.Point(82, 114);
            this.cmdPrijava.Name = "cmdPrijava";
            this.cmdPrijava.Size = new System.Drawing.Size(75, 23);
            this.cmdPrijava.TabIndex = 5;
            this.cmdPrijava.Text = "&Prijava";
            this.cmdPrijava.UseVisualStyleBackColor = true;
            this.cmdPrijava.Click += new System.EventHandler(this.cmdPrijava_Click);
            // 
            // cmdIzlaz
            // 
            this.cmdIzlaz.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdIzlaz.Location = new System.Drawing.Point(163, 114);
            this.cmdIzlaz.Name = "cmdIzlaz";
            this.cmdIzlaz.Size = new System.Drawing.Size(75, 23);
            this.cmdIzlaz.TabIndex = 6;
            this.cmdIzlaz.Text = "I&zlaz";
            this.cmdIzlaz.UseVisualStyleBackColor = true;
            // 
            // frmPrijava
            // 
            this.AcceptButton = this.cmdPrijava;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cmdIzlaz;
            this.ClientSize = new System.Drawing.Size(250, 149);
            this.ControlBox = false;
            this.Controls.Add(this.cmdIzlaz);
            this.Controls.Add(this.cmdPrijava);
            this.Controls.Add(this.txLozinka);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txKorisnickoIme);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "frmPrijava";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Prijava Korisnika";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txKorisnickoIme;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txLozinka;
        private System.Windows.Forms.Button cmdPrijava;
        private System.Windows.Forms.Button cmdIzlaz;
    }
}