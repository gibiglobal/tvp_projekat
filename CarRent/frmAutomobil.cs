﻿using CarRent.Helpers;
using CarRent.Modeli;
using System;
using System.Linq;
using System.Windows.Forms;

namespace CarRent
{
    public partial class frmAutomobil : Form
    {
        private Automobil automobil;

        public frmAutomobil(Automobil automobil)
        {
            InitializeComponent();

            this.automobil = automobil;

            PopuniKontrole();
        }

        private void frmAutomobil_Load(object sender, EventArgs e)
        {
            PopuniAutomobil();
        }

        private void PopuniKontrole()
        {
            // Marke
            var marke = AutomobilHelper.GetSveMarke();
            cbMarka.DataSource = marke;
            cbMarka.DisplayMember = "Marka";
            cbMarka.SelectedIndexChanged += CbMarka_SelectedIndexChanged;

            // Datum
            nudGodiste.Maximum = DateTime.Now.Year;
            nudGodiste.Value = DateTime.Now.Year;

            // Kubikaze
            var kubikaze = AutomobilHelper.GetKubikaze();
            cbKubikaza.DataSource = kubikaze;

            // Pogoni
            var pogoni = AutomobilHelper.GetPogoni();
            cbPogon.DataSource = pogoni;

            // Menjaci
            var menjaci = AutomobilHelper.GetMenjaci();
            cbMenjac.DataSource = menjaci;

            // Karoserije
            var karoserije = AutomobilHelper.GetKaroserije();
            cbKaroserija.DataSource = karoserije;

            // Goriva
            var goriva = AutomobilHelper.GetGoriva();
            cbGorivo.DataSource = goriva;

            cbMarka.Text = automobil.Marka;
            cbModel.Text = automobil.Model;
            nudGodiste.Value = automobil.Godiste;
            cbKubikaza.Text = automobil.Kubikaza;
            cbPogon.Text = automobil.Pogon;
            cbMenjac.Text = automobil.Menjac;
            cbKubikaza.Text = automobil.Kubikaza;
            cbKaroserija.Text = automobil.Karoserija;
            cbGorivo.Text = automobil.Gorivo;
            nudVrata.Value = automobil.BrojVrata;
        }

        private void CbMarka_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItem = (MarkaAuto)cbMarka.SelectedItem;
            if (selectedItem == null) return;
            cbModel.DataSource = selectedItem.Modeli;
        }

        private void PopuniAutomobil()
        {
            if (automobil == null) return;

            txId.Text = automobil.Id.ToString();
            cbMarka.Text = automobil.Marka;
            cbModel.Text = automobil.Model;
            nudGodiste.Value = automobil.Godiste;
            cbKubikaza.Text = automobil.Kubikaza;
            cbPogon.Text = automobil.Pogon;
            cbMenjac.Text = automobil.Menjac;
            cbKaroserija.Text = automobil.Karoserija;
            cbGorivo.Text = automobil.Gorivo;
            nudVrata.Value = automobil.BrojVrata;
        }

        private bool ProveraUnosa()
        {
            if (string.IsNullOrEmpty(cbMarka.Text))
            {
                MessageBox.Show("Molimo izabetite marku!");
                cbMarka.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(cbModel.Text))
            {
                MessageBox.Show("Molimo izabetite model!");
                cbModel.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(cbKubikaza.Text))
            {
                MessageBox.Show("Molimo izabetite kubikažu!");
                cbKubikaza.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(cbPogon.Text))
            {
                MessageBox.Show("Molimo izabetite pogon!");
                cbPogon.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(cbMenjac.Text))
            {
                MessageBox.Show("Molimo izabetite vrstu menjača!");
                cbMenjac.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(cbKaroserija.Text))
            {
                MessageBox.Show("Molimo izabetite karoseriju!");
                cbKaroserija.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(cbGorivo.Text))
            {
                MessageBox.Show("Molimo izabetite gorivo!");
                cbGorivo.Focus();
                return false;
            }

            return true;
        }

        private void cmdOtkazi_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            if (!ProveraUnosa())
            {
                return;
            }

            var parentForm = (frmMain)this.MdiParent;

            if (parentForm == null)
            {
                MessageBox.Show("Forma nema dostupa do podataka!");
                return;
            }

            automobil.Marka = cbMarka.Text;
            automobil.Model = cbModel.Text;
            automobil.Godiste = (int)nudGodiste.Value;
            automobil.Kubikaza = cbKubikaza.Text;
            automobil.Pogon = cbPogon.Text;
            automobil.Menjac = cbMenjac.Text;
            automobil.Karoserija = cbKaroserija.Text;
            automobil.Gorivo = cbGorivo.Text;
            automobil.BrojVrata = (int)nudVrata.Value;

            if (automobil.Id == 0)
            {
                var sledeciId = (parentForm.Automobili.Count > 0 ? parentForm.Automobili.Max(c => c.Id) : 0) + 1;
                automobil.Id = sledeciId;
                parentForm.Automobili.Add(automobil);
            }
            else
            {
                for (int i = 0; i < parentForm.Automobili.Count; i++)
                {
                    if (automobil.Id == parentForm.Automobili[i].Id)
                    {
                        parentForm.Automobili[i] = automobil;
                        break;
                    }
                }
            }

            FileHelper.SnimiUDatoteku(parentForm.AutomobiliFajl, parentForm.Automobili);
            this.Close();
        }
    }
}
