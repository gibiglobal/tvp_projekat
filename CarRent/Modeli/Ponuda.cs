﻿using System;
using System.Xml.Serialization;

namespace CarRent.Modeli
{
    public class Ponuda
    {
        public int Id { get; set; }
        public int AutomobilId { get; set; }
        public DateTime DatumOd { get; set; }
        public DateTime DatumDo { get; set; }
        public decimal CenaDan { get; set; }

        [XmlIgnore]
        public string DisplayName { get {  return string.Format("Datum Od: {1}\tDatum Do: {2}\tCena na dan: {3}",
                AutomobilId, DatumOd.ToString("dd.MM.yyyy"), DatumDo.ToString("dd.MM.yyyy"), CenaDan.ToString("N2")); } }

        public Ponuda()
        {
            DatumOd = DateTime.Now.Date;
            DatumDo = DateTime.Now.Date;
        }

        public override string ToString()
        {
            return string.Format("Automobil ID: {0}\tDatum Od: {1}\tDatum Do: {2}\tCena na dan: {3}",
                AutomobilId, DatumOd.ToString("dd.MM.yyyy"), DatumDo.ToString("dd.MM.yyyy"), CenaDan.ToString("N2"));
        }
    }
}
