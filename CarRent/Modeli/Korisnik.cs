﻿using CarRent.Enums;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace CarRent.Modeli
{
    [XmlInclude(typeof(Administrator))]
    [XmlInclude(typeof(Kupac))]
    public abstract class Korisnik
    {
        public int Id { get; set; }
        public TipKorisnika Tip { get; set; }
        public string KorisnickoIme { get; set; }
        public string Lozinka { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Jmbg { get; set; }
        public DateTime DatumRodjenja { get; set; }
        public string Telefon { get; set; }

        public abstract List<string> GetStavkeMenija();

        public Korisnik()
        {
            DatumRodjenja = DateTime.Now.Date;
        }

        public override string ToString()
        {
            return string.Format("{0}: \tTip: {1}\tKorisničko ime: {2}\tIme: {3}\tPrezime: {4}\tJMBG: {5}\tDatum rođenja: {6}\tTelefon: {7}",
                Id, Tip, KorisnickoIme, Ime, Prezime, Jmbg, DatumRodjenja.ToString("dd.MM.yyyy"), Telefon);
        }
    }
}
