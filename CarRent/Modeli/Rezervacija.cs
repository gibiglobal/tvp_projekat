﻿using System;

namespace CarRent.Modeli
{
    public class Rezervacija
    {
        public int Id { get; set; }
        public int AutomobilId { get; set; }
        public int KupacId { get; set; }
        public DateTime DatumOd { get; set; }
        public DateTime DatumDo { get; set; }
        public decimal Cena { get; set; }

        public Rezervacija()
        {
            DatumOd = DateTime.Now.Date;
            DatumDo = DateTime.Now.Date;
        }

        public override string ToString()
        {
            return string.Format("Automobil ID: {0}\tKupac ID: {1}\tDatum Od: {2}\tDatum Do: {3}\tCena: {4}",
                AutomobilId, KupacId, DatumOd.ToString("dd.MM.yyyy"), DatumDo.ToString("dd.MM.yyyy"), Cena.ToString("N2"));
        }

        public int DanaUMesecu(DateTime monthDate)
        {
            var pocDatum = DatumOd;
            int brDana = 0;
            while (pocDatum < DatumDo)
            {
                if (pocDatum.Month == monthDate.Month && pocDatum.Year == monthDate.Year)
                    brDana++;
                pocDatum = pocDatum.AddDays(1);
            }

            return brDana;
        }
    }
}
