﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRent.Modeli
{
    class MarkaAuto
    {
        public string Marka { get; set; }
        public List<string> Modeli { get; set; }
    }
}
