﻿using System;
using System.Xml.Serialization;

namespace CarRent.Modeli
{
    public class Automobil
    {
        public int Id { get; set; }
        public string Marka { get; set; }
        public string Model { get; set; }
        public int Godiste { get; set; }
        public string Kubikaza { get; set; }
        public string Pogon { get; set; }
        public string Menjac { get; set; }
        public string Karoserija { get; set; }
        public string Gorivo { get; set; }
        public int BrojVrata { get; set; }

        [XmlIgnore]
        public string ImeZaPrikaz { get { return string.Format("{0} {1} ({2})", Marka, Model, Godiste); } }

        public Automobil()
        {
            Godiste = DateTime.Now.Year;
            BrojVrata = 4;
        }

        public override string ToString()
        {
            return string.Format("{0}: \tMarka: {1}\tModel: {2}\tGodište: {3}\tKubikaža: {4}\tPogon: {5}\tMenjač: {6}\tKaroserija: {7}\tGorivo: {8}\tBroj vrata: {9}",
                Id, Marka, Model, Godiste, Kubikaza, Pogon, Menjac, Karoserija, Gorivo, BrojVrata);
        }
    }
}
