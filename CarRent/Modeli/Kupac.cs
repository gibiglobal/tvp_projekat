﻿using CarRent.Enums;
using System.Collections.Generic;

namespace CarRent.Modeli
{
    public class Kupac : Korisnik
    {
        public Kupac()
        {
            base.Tip = TipKorisnika.Kupac;
        }

        public override List<string> GetStavkeMenija()
        {
            return new List<string> { "Rezervacije" };
        }
    }
}
