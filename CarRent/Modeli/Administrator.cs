﻿using CarRent.Enums;
using System.Collections.Generic;

namespace CarRent.Modeli
{
    public class Administrator : Korisnik
    {
        public Administrator()
        {
            base.Tip = TipKorisnika.Administrator;
        }

        public override List<string> GetStavkeMenija()
        {
            return new List<string> { "Automobili", "Kupci", "Ponude", "Rezervacije", "Statistika" };
        }
    }
}
