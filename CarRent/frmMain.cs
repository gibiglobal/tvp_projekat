﻿using CarRent.Enums;
using CarRent.Modeli;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace CarRent
{
    public partial class frmMain : Form
    {
        internal string AutomobiliFajl { get { return "automobili.xml"; } }
        internal string KorisniciFajl { get { return "korisnici.xml"; } }
        internal string PonudeFajl { get { return "ponude.xml"; } }
        internal string RezervacijeFajl { get { return "rezervacije.xml"; } }

        internal List<Automobil> Automobili { get; private set; }
        internal List<Korisnik> Korisnici { get; private set; }
        internal List<Ponuda> Ponude { get; private set; }
        internal List<Rezervacija> Rezervacije { get; private set; }
        internal Korisnik PrijavljeniKorisnik { get; private set; }

        public frmMain()
        {
            InitializeComponent();

            Automobili = new List<Automobil>();
            Korisnici = new List<Korisnik>();
            Ponude = new List<Ponuda>();
            Rezervacije = new List<Rezervacija>();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            UcitajSvePodatke();
            ProveraKorisnika();

            frmPrijava prijavaForm = new frmPrijava(Korisnici);
            var rez = prijavaForm.ShowDialog();

            if (rez == DialogResult.OK)
            {
                PrijavljeniKorisnik = prijavaForm.PrijavljeniKorisnk;
            } 
            else
            {
                Application.Exit();
            }

            FormirajMeni();
        }

        private void UcitajSvePodatke()
        {
            Automobili = FileHelper.ProcitajIzDatoteke<List<Automobil>>(AutomobiliFajl) ?? new List<Automobil>();
            Korisnici = FileHelper.ProcitajIzDatoteke<List<Korisnik>>(KorisniciFajl) ?? new List<Korisnik>();
            Ponude = FileHelper.ProcitajIzDatoteke<List<Ponuda>>(PonudeFajl) ?? new List<Ponuda>();
            Rezervacije = FileHelper.ProcitajIzDatoteke<List<Rezervacija>>(RezervacijeFajl) ?? new List<Rezervacija>();
        }

        private void ProveraKorisnika()
        {
            if (Korisnici.Count == 0)
            {
                MessageBox.Show("Ne postoji ni jedan registrovani korisnik! Za nastavak je potrebno da se unese nalog za Administratora.");

                var korisnik = new Administrator();
                var korisnikForm = new frmKorisnik(korisnik, FormMode.Dodaj, Korisnici);
                korisnikForm.DisableTip();
                //korisnikForm.MdiParent = this;
                korisnikForm.MaximizeBox = false;
                korisnikForm.ControlBox = false;
                var rez = korisnikForm.ShowDialog();

                if (rez != DialogResult.OK)
                {
                    Application.Exit();
                }
            }
        }

        private void FormirajMeni()
        {
            var korisnickiMeni = PrijavljeniKorisnik.GetStavkeMenija();

            if (korisnickiMeni.Contains("Automobili"))
            {
                var miAutomobili = new ToolStripMenuItem();
                miAutomobili.Text = "&Automobili";

                var miAutomobiliDodaj = new ToolStripMenuItem();
                miAutomobiliDodaj.Text = "Dodaj novi";
                miAutomobiliDodaj.Click += MenuItemAutomobilDodaj_Click;
                miAutomobili.DropDownItems.Add(miAutomobiliDodaj);

                var miAutomobiliPregled = new ToolStripMenuItem();
                miAutomobiliPregled.Text = "Pregled automobila";
                miAutomobiliPregled.Click += MenuItemAutomobiliPregled_Click;
                miAutomobili.DropDownItems.Add(miAutomobiliPregled);

                msMenu.Items.Add(miAutomobili);
            }

            if (korisnickiMeni.Contains("Kupci"))
            {
                var miKupci = new ToolStripMenuItem();
                miKupci.Text = "&Kupci";

                var miKupacDodaj = new ToolStripMenuItem();
                miKupacDodaj.Text = "Dodaj novog";
                miKupacDodaj.Click += MenuItemKupacDodaj_Click;
                miKupci.DropDownItems.Add(miKupacDodaj);

                var miKupciPregled = new ToolStripMenuItem();
                miKupciPregled.Text = "Pregled kupaca";
                miKupciPregled.Click += MenuItemKupciPregled_Click;
                miKupci.DropDownItems.Add(miKupciPregled);

                msMenu.Items.Add(miKupci);
            }

            if (korisnickiMeni.Contains("Ponude"))
            {
                var miPonude = new ToolStripMenuItem();
                miPonude.Text = "&Ponude";

                var miPonudaDodaj = new ToolStripMenuItem();
                miPonudaDodaj.Text = "Dodaj novu";
                miPonudaDodaj.Click += MenuItemPonudaDodaj_Click;
                miPonude.DropDownItems.Add(miPonudaDodaj);

                var miPonudePregled = new ToolStripMenuItem();
                miPonudePregled.Text = "Pregled ponuda";
                miPonudePregled.Click += MenuItemPonudePregled_Click;
                miPonude.DropDownItems.Add(miPonudePregled);

                msMenu.Items.Add(miPonude);
            }

            if (korisnickiMeni.Contains("Rezervacije"))
            {
                var miRezervacije = new ToolStripMenuItem();
                miRezervacije.Text = "&Rezervacije";

                var miRezervacijaDodaj = new ToolStripMenuItem();
                miRezervacijaDodaj.Text = "Dodaj novu";
                miRezervacijaDodaj.Click += MenuItemRezervacijaDodaj_Click;
                miRezervacije.DropDownItems.Add(miRezervacijaDodaj);

                var miRezervacijePregled = new ToolStripMenuItem();
                miRezervacijePregled.Text = "Pregled rezervacija";
                miRezervacijePregled.Click += MenuItemRezervacijePregled_Click;
                miRezervacije.DropDownItems.Add(miRezervacijePregled);

                msMenu.Items.Add(miRezervacije);
            }

            if (korisnickiMeni.Contains("Statistika"))
            {
                var miStatistika = new ToolStripMenuItem();
                miStatistika.Text = "&Statistika";
                miStatistika.Click += MenuItemStatistika_Click;

                msMenu.Items.Add(miStatistika);
            }
        }

        #region Menu Items

        private void MenuItemAutomobilDodaj_Click(object sender, EventArgs e)
        {
            DodajAutomobil();
        }

        private void MenuItemAutomobiliPregled_Click(object sender, EventArgs e)
        {
            var pregledForma = new frmPregled<Automobil>(Automobili);
            pregledForma.lbNaslov.Text = "Pregled automobila";
            pregledForma.MdiParent = this;

            pregledForma.OnDodaj = new frmPregled<Automobil>.DodajMethod(DodajAutomobil);
            pregledForma.OnPromeni = new frmPregled<Automobil>.PromeniMethod(PromeniAutomobil);
            pregledForma.OnObrisi = new frmPregled<Automobil>.ObrisiMethod(ObrisiAutomobil);

            pregledForma.Show();
        }

        private void MenuItemKupacDodaj_Click(object sender, EventArgs e)
        {
            DodajKorisnika();
        }

        private void MenuItemKupciPregled_Click(object sender, EventArgs e)
        {
            var pregledForma = new frmPregled<Korisnik>(Korisnici);
            pregledForma.lbNaslov.Text = "Pregled korisnika";
            pregledForma.MdiParent = this;

            pregledForma.OnDodaj = new frmPregled<Korisnik>.DodajMethod(DodajKorisnika);
            pregledForma.OnPromeni = new frmPregled<Korisnik>.PromeniMethod(PromeniKorisnika);
            pregledForma.OnObrisi = new frmPregled<Korisnik>.ObrisiMethod(ObrisiKorisnika);

            pregledForma.Show();
        }

        private void MenuItemPonudaDodaj_Click(object sender, EventArgs e)
        {
            DodajPonudu();
        }

        private void MenuItemPonudePregled_Click(object sender, EventArgs e)
        {
            var pregledForma = new frmPregled<Ponuda>(Ponude);
            pregledForma.lbNaslov.Text = "Pregled ponuda";
            pregledForma.MdiParent = this;

            pregledForma.OnDodaj = new frmPregled<Ponuda>.DodajMethod(DodajPonudu);
            pregledForma.OnPromeni = new frmPregled<Ponuda>.PromeniMethod(PromeniPonudu);
            pregledForma.OnObrisi = new frmPregled<Ponuda>.ObrisiMethod(ObrisiPonudu);

            pregledForma.Show();
        }

        private void MenuItemRezervacijaDodaj_Click(object sender, EventArgs e)
        {
            DodajRezervaciju();
        }

        private void MenuItemRezervacijePregled_Click(object sender, EventArgs e)
        {
            List<Rezervacija> rezervacije;
            if (PrijavljeniKorisnik.Tip == TipKorisnika.Administrator) // vidi sve
            {
                rezervacije = Rezervacije;
            }
            else
            {
                rezervacije = this.Rezervacije.FindAll(f => f.KupacId == PrijavljeniKorisnik.Id);
            }

            var pregledForma = new frmPregled<Rezervacija>(rezervacije);
            pregledForma.lbNaslov.Text = "Pregled rezervacija";
            pregledForma.MdiParent = this;

            pregledForma.OnDodaj = new frmPregled<Rezervacija>.DodajMethod(DodajRezervaciju);
            pregledForma.OnPromeni = new frmPregled<Rezervacija>.PromeniMethod(PromeniRezervaciju);
            pregledForma.OnObrisi = new frmPregled<Rezervacija>.ObrisiMethod(ObrisiRezervaciju);

            pregledForma.Show();
        }

        private void MenuItemStatistika_Click(object sender, EventArgs e)
        {
            var statistikaForma = new frmStatistika();
            statistikaForma.MdiParent = this;
            statistikaForma.Show();
        }

        #endregion

        #region Delegate metode

        // Automobil
        public void DodajAutomobil()
        {
            var automobil = new Automobil();
            var automobilForm = new frmAutomobil(automobil);
            automobilForm.MdiParent = this;
            automobilForm.Show();
        }

        public void ObrisiAutomobil(Automobil automobil)
        {
            Automobili.Remove(automobil);
            FileHelper.SnimiUDatoteku(AutomobiliFajl, Automobili);
        }

        public void PromeniAutomobil(Automobil automobil)
        {
            var automobilForm = new frmAutomobil(automobil);
            automobilForm.MdiParent = this;
            automobilForm.Show();
        }

        // Korisnik
        public void DodajKorisnika()
        {
            var korisnik = new Kupac();
            var korisnikForm = new frmKorisnik(korisnik, FormMode.Dodaj, Korisnici);
            korisnikForm.MdiParent = this;
            korisnikForm.Show();
        }

        public void ObrisiKorisnika(Korisnik korisnik)
        {
            Korisnici.Remove(korisnik);
            FileHelper.SnimiUDatoteku(KorisniciFajl, Korisnici);
        }

        public void PromeniKorisnika(Korisnik korisnik)
        {
            var korisnikForm = new frmKorisnik(korisnik, FormMode.Uredi, Korisnici);
            korisnikForm.MdiParent = this;
            korisnikForm.Show();
        }

        // Ponuda
        public void DodajPonudu()
        {
            var ponuda = new Ponuda();
            var ponudaForm = new frmPonuda(ponuda);
            ponudaForm.MdiParent = this;
            ponudaForm.Show();
        }

        public void ObrisiPonudu(Ponuda ponuda)
        {
            Ponude.Remove(ponuda);
            FileHelper.SnimiUDatoteku(PonudeFajl, Ponude);
        }

        public void PromeniPonudu(Ponuda ponuda)
        {
            var ponudaForm = new frmPonuda(ponuda);
            ponudaForm.MdiParent = this;
            ponudaForm.Show();
        }

        // Rezervacija
        public void DodajRezervaciju()
        {
            var rezervacija = new Rezervacija();
            rezervacija.KupacId = PrijavljeniKorisnik.Id;
            var rezervacijaForm = new frmRezervacija(rezervacija);
            rezervacijaForm.MdiParent = this;
            rezervacijaForm.Show();
        }

        public void ObrisiRezervaciju(Rezervacija rezervacija)
        {
            Rezervacije.Remove(rezervacija);
            FileHelper.SnimiUDatoteku(RezervacijeFajl, Rezervacije);
        }

        public void PromeniRezervaciju(Rezervacija rezervacija)
        {
            var rezervacijaForm = new frmRezervacija(rezervacija);
            rezervacijaForm.MdiParent = this;
            rezervacijaForm.Show();
        }

        #endregion
    }
}
