﻿using CarRent.Modeli;
using System;
using System.Linq;
using System.Windows.Forms;

namespace CarRent
{
    public partial class frmPonuda : Form
    {
        private Ponuda ponuda;

        public frmPonuda(Ponuda ponuda)
        {
            InitializeComponent();

            this.ponuda = ponuda;    
        }

        private void frmPonuda_Load(object sender, EventArgs e)
        {
            PopuniKontrole();
            PopuniPonudu();
        }

        private void PopuniKontrole()
        {
            var parentForm = (frmMain)this.MdiParent;

            if (parentForm == null)
            {
                MessageBox.Show("Forma nema dostupa do podataka!");
                return;
            }

            // Automobil
            var automobili = parentForm.Automobili;
            var bindingSource = new BindingSource();
            bindingSource.DataSource = automobili;
            cbAutomobil.DataSource = bindingSource.DataSource;
            cbAutomobil.DisplayMember = "ImeZaPrikaz";
            cbAutomobil.ValueMember = "Id";

            // Datumi
            if (ponuda.Id == 0)
            {
                dtDatumOd.MinDate = DateTime.Now.Date;
                dtDatumDo.MinDate = DateTime.Now.Date;
            }
        }

        private void PopuniPonudu()
        {
            if (ponuda == null) return;

            if (ponuda.AutomobilId == 0)
                return;

            cbAutomobil.SelectedValue = ponuda.AutomobilId;
            dtDatumOd.Value = ponuda.DatumOd.Date;
            dtDatumDo.Value = ponuda.DatumDo.Date;
            nudCena.Value = ponuda.CenaDan;
        }

        private bool ProveraUnosa()
        {
            if (cbAutomobil.SelectedValue == null)
            {
                MessageBox.Show("Molimo izabetite automobil!");
                cbAutomobil.Focus();
                return false;
            }

            if (dtDatumDo.Value < dtDatumOd.Value)
            {
                MessageBox.Show("Krajnji datum mora biti veći ili jednak početnom!");
                dtDatumDo.Focus();
                return false;
            }

            if (nudCena.Value <= 0)
            {
                MessageBox.Show("Morate uneci cenu automobila na dan!/nCena mora biti veća od 0.");
                return false;
            }

            return true;
        }

        private void cmdOtkazi_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            if (!ProveraUnosa())
            {
                return;
            }

            var parentForm = (frmMain)this.MdiParent;

            if (parentForm == null)
            {
                MessageBox.Show("Forma nema dostupa do podataka!");
                return;
            }

            ponuda.AutomobilId = (int)cbAutomobil.SelectedValue;
            ponuda.DatumOd = dtDatumOd.Value;
            ponuda.DatumDo = dtDatumDo.Value;
            ponuda.CenaDan = nudCena.Value;

            if (ponuda.Id == 0)
            {
                var sledeciId = (parentForm.Ponude.Count > 0 ? parentForm.Ponude.Max(c => c.Id) : 0) + 1;
                ponuda.Id = sledeciId;
                parentForm.Ponude.Add(ponuda);
            }
            else
            {
                for (int i = 0; i < parentForm.Ponude.Count; i++)
                {
                    if (ponuda.Id == parentForm.Ponude[i].Id)
                    {
                        parentForm.Ponude[i] = ponuda;
                        break;
                    }
                }
            }

            FileHelper.SnimiUDatoteku(parentForm.PonudeFajl, parentForm.Ponude);
            this.Close();
        }
    }
}
