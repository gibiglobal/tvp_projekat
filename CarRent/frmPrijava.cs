﻿using CarRent.Enums;
using CarRent.Modeli;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace CarRent
{
    public partial class frmPrijava : Form
    {
        internal Korisnik PrijavljeniKorisnk { get; private set; }
        List<Korisnik> korisnici;

        public frmPrijava(List<Korisnik> korisnici)
        {
            InitializeComponent();
            this.korisnici = korisnici;
        }

        private void cmdPrijava_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txKorisnickoIme.Text))
            {
                MessageBox.Show("Molimo unesite korisničko ime!");
                txKorisnickoIme.Focus();
                return;
            }

            if (string.IsNullOrEmpty(txLozinka.Text))
            {
                MessageBox.Show("Molimo unesite lozinku!");
                txLozinka.Focus();
                return;
            }

            foreach (var korisnik in korisnici)
            {
                if (korisnik.KorisnickoIme == txKorisnickoIme.Text && korisnik.Lozinka == txLozinka.Text)
                {
                    if (korisnik.Tip == TipKorisnika.Administrator)
                        PrijavljeniKorisnk = (Administrator)korisnik;
                    else
                        PrijavljeniKorisnk = (Kupac)korisnik;

                    this.DialogResult = DialogResult.OK;
                    this.Close();
                    return;
                }
            }

            MessageBox.Show("Korisničko ime ili lozinka nisu ispravni!\nMolimo pokušajte ponovo.");
        }
    }
}
