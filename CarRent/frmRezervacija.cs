﻿using CarRent.Helpers;
using CarRent.Modeli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace CarRent
{
    public partial class frmRezervacija : Form
    {
        private Rezervacija rezervacija;
        private Automobil automobil;
        private List<Ponuda> autoPonude;


        public frmRezervacija(Rezervacija rezervacija)
        {
            InitializeComponent();

            this.rezervacija = rezervacija;
        }

        private void frmRezervacija_Load(object sender, EventArgs e)
        {
            if (rezervacija.AutomobilId > 0)
                automobil = GetAutomobilById(rezervacija.AutomobilId);

            PopuniKontrole();

            if (rezervacija.Id == 0) // Nova rezervacija
                PopuniRezervaciju();
        }

        private void PopuniKontrole()
        {
            // Marke
            var marke = AutomobilHelper.GetSveMarke();
            MarkaAuto selectedMarka = null;
            if (automobil != null)
            {
                selectedMarka = marke.FindAll(f => f.Marka == automobil.Marka)[0];
                
            }

            cbMarka.DisplayMember = "Marka";
            cbMarka.DataSource = marke;
            cbMarka.SelectedItem = selectedMarka;
        }

        private void PopuniRezervaciju()
        {
            // popuni ponude
            PopuniPonude();

            dtRezOd.Value = rezervacija.DatumOd;
            dtRezDo.Value = rezervacija.DatumDo;
            txCena.Text = rezervacija.Cena.ToString("N2");
        }

        private void PopuniPonude()
        {
            // popuni ponude
            if (automobil != null)
                autoPonude = GetPonudeByAutoId(automobil.Id);
            else
                autoPonude = null;

            lbPonude.DisplayMember = "DisplayName";
            lbPonude.ValueMember = "Id";
            lbPonude.DataSource = autoPonude;
        }

        private void FilterConbo_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cb = (ComboBox)sender;
            if (cb == null) return;

            Filtriraj(cb.Name);
        }

        private void Filtriraj(string ctlName)
        {
            var parentForm = (frmMain)this.MdiParent;

            if (parentForm == null)
            {
                MessageBox.Show("Forma nema dostupa do podataka!");
                return;
            }

            int ctlBrojac = 1;
            List<Automobil> filteredAutomobili;

            switch (ctlName)
            {
                case "cbMarka":
                    if (cbModel.SelectedIndex >= 0) ResetFilterRezultat();

                    var selectedMarka = automobil != null
                        ? automobil.Marka
                        : (cbMarka.SelectedItem != null ? ((MarkaAuto)cbMarka.SelectedItem).Marka : "");

                    filteredAutomobili = parentForm.Automobili.FindAll(f => f.Marka == selectedMarka);

                    if (filteredAutomobili.Count == 0)
                    {
                        cbModel.DataSource = null;
                        return;
                    }
                    else
                    {
                        List<string> modeli = new List<string>();
                        foreach (var auto in filteredAutomobili)
                        {
                            if (!modeli.Contains(auto.Model))
                                modeli.Add(auto.Model);
                        }
                        cbModel.DataSource = modeli;

                    }
                    break;

                case "cbModel":
                    if (cbGodiste.SelectedIndex >= 0) ResetFilterRezultat();

                    var selectedModel = automobil != null
                        ? automobil.Model
                        : (string)cbModel.SelectedItem;

                    filteredAutomobili = parentForm.Automobili.FindAll(f => f.Marka == cbMarka.Text &&
                        f.Model == selectedModel);

                    if (filteredAutomobili.Count == 0)
                    {
                        cbGodiste.DataSource = null;
                        return;
                    }
                    else
                    {
                        List<string> godista = new List<string>();
                        foreach (var auto in filteredAutomobili)
                        {
                            if (!godista.Contains(auto.Godiste.ToString()))
                                godista.Add(auto.Godiste.ToString());
                        }

                        cbGodiste.DataSource = godista;
                    }
                    break;

                case "cbGodiste":
                    if (cbKubikaza.SelectedIndex >= 0) ResetFilterRezultat();

                    var selectedGodiste = automobil != null
                        ? automobil.Godiste.ToString()
                        : (string)cbGodiste.SelectedItem;

                    filteredAutomobili = parentForm.Automobili.FindAll(f => f.Marka == cbMarka.Text
                        && f.Model == cbModel.Text
                        && f.Godiste == int.Parse(selectedGodiste));

                    if (filteredAutomobili.Count == 0)
                    {
                        cbKubikaza.DataSource = null;
                        return;
                    }
                    else
                    {
                        List<string> kubikaze = new List<string>();
                        foreach (var auto in filteredAutomobili)
                        {
                            if (!kubikaze.Contains(auto.Kubikaza))
                                kubikaze.Add(auto.Kubikaza);
                        }

                        cbKubikaza.DataSource = kubikaze;
                    }
                    break;

                case "cbKubikaza":
                    if (cbKaroserija.SelectedIndex >= 0) ResetFilterRezultat();

                    var selectedKubikaza = automobil != null
                        ? automobil.Kubikaza
                        : (string)cbKubikaza.SelectedItem;

                    filteredAutomobili = parentForm.Automobili.FindAll(f => f.Marka == cbMarka.Text
                        && f.Model == cbModel.Text
                        && f.Godiste == int.Parse(cbGodiste.Text)
                        && f.Kubikaza == selectedKubikaza);

                    if (filteredAutomobili.Count == 0)
                    {
                        cbKaroserija.DataSource = null;
                        return;
                    }
                    else
                    {
                        List<string> karoserije = new List<string>();
                        foreach (var auto in filteredAutomobili)
                        {
                            if (!karoserije.Contains(auto.Karoserija))
                                karoserije.Add(auto.Karoserija);
                        }

                        cbKaroserija.DataSource = karoserije;
                    }
                    break;

                case "cbKaroserija":
                    if (cbBrojVrata.SelectedIndex >= 0) ResetFilterRezultat();

                    var selectedKaroserija = automobil != null
                        ? automobil.Karoserija
                        : (string)cbKaroserija.SelectedItem;

                    filteredAutomobili = parentForm.Automobili.FindAll(f => f.Marka == cbMarka.Text
                        && f.Model == cbModel.Text
                        && f.Godiste == int.Parse(cbGodiste.Text)
                        && f.Kubikaza == cbKubikaza.Text
                        && f.Karoserija == selectedKaroserija);

                    if (filteredAutomobili.Count == 0)
                    {
                        cbBrojVrata.DataSource = null;
                        return;
                    }
                    else
                    {
                        List<string> brojeviVrata = new List<string>();
                        foreach (var auto in filteredAutomobili)
                        {
                            if (!brojeviVrata.Contains(auto.BrojVrata.ToString()))
                                brojeviVrata.Add(auto.BrojVrata.ToString());
                        }

                        cbBrojVrata.DataSource = brojeviVrata;
                    }
                    break;

                case "cbBrojVrata":
                    if (cbGorivo.SelectedIndex >= 0) ResetFilterRezultat();

                    var selectedBrVrata = automobil != null
                        ? automobil.BrojVrata.ToString()
                        : (string)cbBrojVrata.SelectedItem;

                    filteredAutomobili = parentForm.Automobili.FindAll(f => f.Marka == cbMarka.Text
                        && f.Model == cbModel.Text
                        && f.Godiste == int.Parse(cbGodiste.Text)
                        && f.Kubikaza == cbKubikaza.Text
                        && f.Karoserija == cbKaroserija.Text
                        && f.BrojVrata == int.Parse(selectedBrVrata));

                    if (filteredAutomobili.Count == 0)
                    {
                        cbGorivo.DataSource = null;
                        return;
                    }
                    else
                    {
                        List<string> goriva = new List<string>();
                        foreach (var auto in filteredAutomobili)
                        {
                            if (!goriva.Contains(auto.Gorivo))
                                goriva.Add(auto.Gorivo);
                        }

                        cbGorivo.DataSource = goriva;
                    }
                    break;

                case "cbGorivo":
                    if (cbPogon.SelectedIndex >= 0) ResetFilterRezultat();

                    var selectedGorivo = automobil != null
                        ? automobil.Gorivo
                        : (string)cbGorivo.SelectedItem;

                    filteredAutomobili = parentForm.Automobili.FindAll(f => f.Marka == cbMarka.Text
                        && f.Model == cbModel.Text
                        && f.Godiste == int.Parse(cbGodiste.Text)
                        && f.Kubikaza == cbKubikaza.Text
                        && f.Karoserija == cbKaroserija.Text
                        && f.BrojVrata == int.Parse(cbBrojVrata.Text)
                        && f.Gorivo == selectedGorivo);

                    if (filteredAutomobili.Count == 0)
                    {
                        cbPogon.DataSource = null;
                        return;
                    }
                    else
                    {
                        List<string> pogoni = new List<string>();
                        foreach (var auto in filteredAutomobili)
                        {
                            if (!pogoni.Contains(auto.Pogon))
                                pogoni.Add(auto.Pogon);
                        }

                        cbPogon.DataSource = pogoni;
                    }
                    break;

                case "cbPogon":
                    if (cbMenjac.SelectedIndex >= 0) ResetFilterRezultat();

                    var selectedPogon = automobil != null
                        ? automobil.Pogon
                        : (string)cbPogon.SelectedItem;

                    filteredAutomobili = parentForm.Automobili.FindAll(f => f.Marka == cbMarka.Text
                        && f.Model == cbModel.Text
                        && f.Godiste == int.Parse(cbGodiste.Text)
                        && f.Kubikaza == cbKubikaza.Text
                        && f.Karoserija == cbKaroserija.Text
                        && f.BrojVrata == int.Parse(cbBrojVrata.Text)
                        && f.Gorivo == cbGorivo.Text
                        && f.Pogon == selectedPogon);

                    if (filteredAutomobili.Count == 0)
                    {
                        cbMenjac.DataSource = null;
                        return;
                    }
                    else
                    {
                        List<string> menjaci = new List<string>();
                        foreach (var auto in filteredAutomobili)
                        {
                            if (!menjaci.Contains(auto.Menjac))
                                menjaci.Add(auto.Menjac);
                        }

                        cbMenjac.DataSource = menjaci;
                    }
                    break;

                case "cbMenjac":
                    var selectedMenjac = automobil != null
                        ? automobil.Menjac
                        : (string)cbMenjac.SelectedItem;

                    filteredAutomobili = parentForm.Automobili.FindAll(f => f.Marka == cbMarka.Text
                        && f.Model == cbModel.Text
                        && f.Godiste == int.Parse(cbGodiste.Text)
                        && f.Kubikaza == cbKubikaza.Text
                        && f.Karoserija == cbKaroserija.Text
                        && f.BrojVrata == int.Parse(cbBrojVrata.Text)
                        && f.Gorivo == cbGorivo.Text
                        && f.Pogon == cbPogon.Text
                        && f.Menjac == selectedMenjac);

                    if (filteredAutomobili.Count == 0)
                    {
                        ResetFilterRezultat();
                        MessageBox.Show("Nije pronađen ni jedan automobil!\nPokušajte sa drugim filterom.");
                        return;
                    }
                    else
                    {
                        automobil = filteredAutomobili[0];
                    }
                    break;
            }
        }

        private void ResetFilterRezultat()
        {
            lbPonude.SelectedIndex = -1;
            automobil = null;
            autoPonude = null;
        }

        private Automobil GetAutomobilById(int autoId)
        {
            var parentForm = (frmMain)this.MdiParent;

            if (parentForm == null)
            {
                MessageBox.Show("Forma nema dostupa do podataka!");
                return null;
            }

            var filteredAuto = parentForm.Automobili.FindAll(f => f.Id == autoId);
            if (filteredAuto.Count > 0) return filteredAuto[0];

            return null;
        }

        private List<Ponuda> GetPonudeByAutoId(int autoId)
        {
            var parentForm = (frmMain)this.MdiParent;

            if (parentForm == null)
            {
                MessageBox.Show("Forma nema dostupa do podataka!");
                return null;
            }

            var filteredPonude = parentForm.Ponude.FindAll(f => f.AutomobilId == autoId);
            return filteredPonude;
        }

        private Ponuda GetIzabranaPonuda()
        {
            if (lbPonude.SelectedIndex == -1)
                return null;

            var ponuda = (Ponuda)lbPonude.SelectedItem;
            return ponuda;
        }

        private void btnPrikaz_Click(object sender, EventArgs e)
        {
            if (automobil == null)
            {
                MessageBox.Show("Nije izabran automobil! Izaberite automobil, pa pokušajte ponovo");
                cbMarka.Focus();
                return;
            }
            PopuniPonude();
        }

        private bool ProveraUnosa()
        {
            if (automobil == null)
            {
                MessageBox.Show("Nije izabran automobil! Izaberite automobil, pa pokušajte ponovo");
                cbMarka.Focus();
                return false;
            }

            var ponuda = GetIzabranaPonuda();
            if (ponuda == null)
            {
                MessageBox.Show("Nije izabrana ponuda! Najpre prikažite i izaberite automobil, pa pokušajte ponovo");
                lbPonude.Focus();
                return false;
            }
            
            if (dtRezOd.Value.Date < ponuda.DatumOd.Date)
            {
                MessageBox.Show("Datum početka rezervacije ne može biti manji od početka ponude!");
                dtRezOd.Focus();
                return false;
            }

            if (dtRezOd.Value.Date > dtRezDo.Value.Date)
            {
                MessageBox.Show("Datum početka rezervacije ne može veći od  manji od datuma završetka rezervacije!");
                dtRezOd.Focus();
                return false;
            }

            if (dtRezDo.Value.Date > ponuda.DatumDo.Date)
            {
                MessageBox.Show("Datum završetka rezervacije ne može biti veći od kraja ponude!");
                dtRezDo.Focus();
                return false;
            }

            if (dtRezDo.Value.Date < dtRezOd.Value.Date)
            {
                MessageBox.Show("Datum završetka rezervacije ne može biti manji od datuma početka rezervacije!");
                dtRezDo.Focus();
                return false;
            }

            return true;
        }

        private void lbPonude_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbPonude.SelectedIndex == -1) return;

            var ponuda = GetIzabranaPonuda();
            if (ponuda == null) return;

            dtRezOd.MinDate = ponuda.DatumOd;
            dtRezOd.MaxDate = ponuda.DatumDo;
            dtRezDo.MinDate = ponuda.DatumOd;
            dtRezDo.MaxDate = ponuda.DatumDo;
        }

        private void btnRezervisi_Click(object sender, EventArgs e)
        {
            if (!ProveraUnosa())
            {
                return;
            }

            var parentForm = (frmMain)this.MdiParent;

            if (parentForm == null)
            {
                MessageBox.Show("Forma nema dostupa do podataka!");
                return;
            }

            rezervacija.AutomobilId = automobil.Id;
            rezervacija.DatumOd = dtRezOd.Value;
            rezervacija.DatumDo = dtRezDo.Value;
            IzracunajCenu();

            if (rezervacija.Id == 0)
            {
                var sledeciId = (parentForm.Rezervacije.Count > 0 ? parentForm.Rezervacije.Max(c => c.Id) : 0) + 1;
                rezervacija.Id = sledeciId;
                parentForm.Rezervacije.Add(rezervacija);
            }
            else
            {
                for (int i = 0; i < parentForm.Rezervacije.Count; i++)
                {
                    if (rezervacija.Id == parentForm.Rezervacije[i].Id)
                    {
                        parentForm.Rezervacije[i] = rezervacija;
                        break;
                    }
                }
            }

            FileHelper.SnimiUDatoteku(parentForm.RezervacijeFajl, parentForm.Rezervacije);
            this.Close();
        }

        private void IzracunajCenu()
        {
            var ponuda = GetIzabranaPonuda();
            if (ponuda == null) return;

            rezervacija.Cena = (decimal)(dtRezDo.Value.Date - dtRezOd.Value.Date).TotalDays * ponuda.CenaDan;
            txCena.Text = rezervacija.Cena.ToString("N2");
        }

        private void datum_ValueChanged(object sender, EventArgs e)
        {
            if (dtRezOd.Value.Date > dtRezDo.Value.Date)
            {
                MessageBox.Show("Datum početka rezervacije ne može veći od  manji od datuma završetka rezervacije!");
                dtRezOd.Focus();
            }

            if (dtRezDo.Value.Date < dtRezOd.Value.Date)
            {
                MessageBox.Show("Datum završetka rezervacije ne može biti manji od datuma početka rezervacije!");
                dtRezDo.Focus();
            }
            IzracunajCenu();
        }

        
    }
}
