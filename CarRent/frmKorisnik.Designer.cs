﻿namespace CarRent
{
    partial class frmKorisnik
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txTelefon = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dtDatumRodjenja = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.txJmbg = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txPrezime = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txIme = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txLozinka2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txLozinka = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txKorisnickoIme = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbAdmin = new System.Windows.Forms.RadioButton();
            this.rbKupac = new System.Windows.Forms.RadioButton();
            this.cmdOk = new System.Windows.Forms.Button();
            this.cmdOtkazi = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txTelefon);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.dtDatumRodjenja);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txJmbg);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txPrezime);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txIme);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(318, 163);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Osnovni podaci";
            // 
            // txTelefon
            // 
            this.txTelefon.Location = new System.Drawing.Point(92, 131);
            this.txTelefon.Name = "txTelefon";
            this.txTelefon.Size = new System.Drawing.Size(220, 20);
            this.txTelefon.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Telefon:";
            // 
            // dtDatumRodjenja
            // 
            this.dtDatumRodjenja.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDatumRodjenja.Location = new System.Drawing.Point(92, 105);
            this.dtDatumRodjenja.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtDatumRodjenja.Name = "dtDatumRodjenja";
            this.dtDatumRodjenja.Size = new System.Drawing.Size(220, 20);
            this.dtDatumRodjenja.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Datum rođenja:";
            // 
            // txJmbg
            // 
            this.txJmbg.Location = new System.Drawing.Point(92, 79);
            this.txJmbg.MaxLength = 13;
            this.txJmbg.Name = "txJmbg";
            this.txJmbg.Size = new System.Drawing.Size(220, 20);
            this.txJmbg.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "JMBG:";
            // 
            // txPrezime
            // 
            this.txPrezime.Location = new System.Drawing.Point(92, 53);
            this.txPrezime.Name = "txPrezime";
            this.txPrezime.Size = new System.Drawing.Size(220, 20);
            this.txPrezime.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Prezime:";
            // 
            // txIme
            // 
            this.txIme.Location = new System.Drawing.Point(92, 27);
            this.txIme.Name = "txIme";
            this.txIme.Size = new System.Drawing.Size(220, 20);
            this.txIme.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ime:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txLozinka2);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txLozinka);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txKorisnickoIme);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(12, 190);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(318, 114);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Podaci za prijavu";
            // 
            // txLozinka2
            // 
            this.txLozinka2.Location = new System.Drawing.Point(92, 81);
            this.txLozinka2.Name = "txLozinka2";
            this.txLozinka2.PasswordChar = '*';
            this.txLozinka2.Size = new System.Drawing.Size(220, 20);
            this.txLozinka2.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 84);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Ponovi lozinku:";
            // 
            // txLozinka
            // 
            this.txLozinka.Location = new System.Drawing.Point(92, 55);
            this.txLozinka.Name = "txLozinka";
            this.txLozinka.PasswordChar = '*';
            this.txLozinka.Size = new System.Drawing.Size(220, 20);
            this.txLozinka.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Lozinka:";
            // 
            // txKorisnickoIme
            // 
            this.txKorisnickoIme.Location = new System.Drawing.Point(92, 29);
            this.txKorisnickoIme.Name = "txKorisnickoIme";
            this.txKorisnickoIme.Size = new System.Drawing.Size(220, 20);
            this.txKorisnickoIme.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Korisničko ime:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbAdmin);
            this.groupBox3.Controls.Add(this.rbKupac);
            this.groupBox3.Location = new System.Drawing.Point(355, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 83);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tip korisnika";
            // 
            // rbAdmin
            // 
            this.rbAdmin.AutoSize = true;
            this.rbAdmin.Location = new System.Drawing.Point(6, 54);
            this.rbAdmin.Name = "rbAdmin";
            this.rbAdmin.Size = new System.Drawing.Size(85, 17);
            this.rbAdmin.TabIndex = 1;
            this.rbAdmin.TabStop = true;
            this.rbAdmin.Text = "Administrator";
            this.rbAdmin.UseVisualStyleBackColor = true;
            // 
            // rbKupac
            // 
            this.rbKupac.AutoSize = true;
            this.rbKupac.Location = new System.Drawing.Point(6, 28);
            this.rbKupac.Name = "rbKupac";
            this.rbKupac.Size = new System.Drawing.Size(56, 17);
            this.rbKupac.TabIndex = 0;
            this.rbKupac.TabStop = true;
            this.rbKupac.Text = "Kupac";
            this.rbKupac.UseVisualStyleBackColor = true;
            // 
            // cmdOk
            // 
            this.cmdOk.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdOk.Location = new System.Drawing.Point(485, 245);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(75, 23);
            this.cmdOk.TabIndex = 3;
            this.cmdOk.Text = "&Ok";
            this.cmdOk.UseVisualStyleBackColor = true;
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // cmdOtkazi
            // 
            this.cmdOtkazi.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdOtkazi.Location = new System.Drawing.Point(485, 274);
            this.cmdOtkazi.Name = "cmdOtkazi";
            this.cmdOtkazi.Size = new System.Drawing.Size(75, 23);
            this.cmdOtkazi.TabIndex = 4;
            this.cmdOtkazi.Text = "O&tkaži";
            this.cmdOtkazi.UseVisualStyleBackColor = true;
            this.cmdOtkazi.Click += new System.EventHandler(this.cmdOtkazi_Click);
            // 
            // frmKorisnik
            // 
            this.AcceptButton = this.cmdOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cmdOtkazi;
            this.ClientSize = new System.Drawing.Size(572, 319);
            this.Controls.Add(this.cmdOtkazi);
            this.Controls.Add(this.cmdOk);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmKorisnik";
            this.ShowInTaskbar = false;
            this.Text = "Podaci korisnika";
            this.Load += new System.EventHandler(this.frmKorisnik_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DateTimePicker dtDatumRodjenja;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txJmbg;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txPrezime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txIme;
        private System.Windows.Forms.TextBox txTelefon;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txLozinka2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txLozinka;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txKorisnickoIme;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton rbAdmin;
        private System.Windows.Forms.RadioButton rbKupac;
        private System.Windows.Forms.Button cmdOk;
        private System.Windows.Forms.Button cmdOtkazi;
    }
}