﻿namespace CarRent
{
    partial class frmAutomobil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nudVrata = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.cbGorivo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbKaroserija = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbMenjac = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbPogon = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbKubikaza = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.nudGodiste = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.cbModel = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbMarka = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdOtkazi = new System.Windows.Forms.Button();
            this.cmdOk = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudVrata)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGodiste)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nudVrata);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.cbGorivo);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.cbKaroserija);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cbMenjac);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cbPogon);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbKubikaza);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.nudGodiste);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbModel);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbMarka);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txId);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 3, 8, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox1.Size = new System.Drawing.Size(445, 309);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Osnovni podaci";
            // 
            // nudVrata
            // 
            this.nudVrata.Location = new System.Drawing.Point(85, 272);
            this.nudVrata.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.nudVrata.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudVrata.Name = "nudVrata";
            this.nudVrata.Size = new System.Drawing.Size(69, 20);
            this.nudVrata.TabIndex = 19;
            this.nudVrata.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudVrata.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 274);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Broj vrata:";
            // 
            // cbGorivo
            // 
            this.cbGorivo.FormattingEnabled = true;
            this.cbGorivo.Location = new System.Drawing.Point(85, 245);
            this.cbGorivo.Name = "cbGorivo";
            this.cbGorivo.Size = new System.Drawing.Size(347, 21);
            this.cbGorivo.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 248);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Gorivo:";
            // 
            // cbKaroserija
            // 
            this.cbKaroserija.FormattingEnabled = true;
            this.cbKaroserija.Location = new System.Drawing.Point(85, 218);
            this.cbKaroserija.Name = "cbKaroserija";
            this.cbKaroserija.Size = new System.Drawing.Size(347, 21);
            this.cbKaroserija.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 221);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Karoserija:";
            // 
            // cbMenjac
            // 
            this.cbMenjac.FormattingEnabled = true;
            this.cbMenjac.Location = new System.Drawing.Point(85, 191);
            this.cbMenjac.Name = "cbMenjac";
            this.cbMenjac.Size = new System.Drawing.Size(347, 21);
            this.cbMenjac.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 194);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Vrsta menjača:";
            // 
            // cbPogon
            // 
            this.cbPogon.FormattingEnabled = true;
            this.cbPogon.Location = new System.Drawing.Point(85, 164);
            this.cbPogon.Name = "cbPogon";
            this.cbPogon.Size = new System.Drawing.Size(347, 21);
            this.cbPogon.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 167);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Pogon:";
            // 
            // cbKubikaza
            // 
            this.cbKubikaza.FormattingEnabled = true;
            this.cbKubikaza.Location = new System.Drawing.Point(85, 137);
            this.cbKubikaza.Name = "cbKubikaza";
            this.cbKubikaza.Size = new System.Drawing.Size(347, 21);
            this.cbKubikaza.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Kubikaža:";
            // 
            // nudGodiste
            // 
            this.nudGodiste.Location = new System.Drawing.Point(85, 113);
            this.nudGodiste.Maximum = new decimal(new int[] {
            2019,
            0,
            0,
            0});
            this.nudGodiste.Minimum = new decimal(new int[] {
            1930,
            0,
            0,
            0});
            this.nudGodiste.Name = "nudGodiste";
            this.nudGodiste.Size = new System.Drawing.Size(69, 20);
            this.nudGodiste.TabIndex = 7;
            this.nudGodiste.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudGodiste.Value = new decimal(new int[] {
            1930,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Godište:";
            // 
            // cbModel
            // 
            this.cbModel.FormattingEnabled = true;
            this.cbModel.Location = new System.Drawing.Point(85, 84);
            this.cbModel.Name = "cbModel";
            this.cbModel.Size = new System.Drawing.Size(347, 21);
            this.cbModel.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Model:";
            // 
            // cbMarka
            // 
            this.cbMarka.FormattingEnabled = true;
            this.cbMarka.Items.AddRange(new object[] {
            "Alfa Romeo",
            "Aro",
            "Aston Martin",
            "Audi",
            "Austin",
            "Bentley",
            "BMW",
            "Buick",
            "Cadillac",
            "Chery",
            "Chevrolet",
            "Chrysler",
            "Citroen",
            "Dacia",
            "Daewoo",
            "Daihatsu",
            "Dodge",
            "DR",
            "Ferrari",
            "Fiat",
            "Ford",
            "GAZ",
            "Great Wall",
            "GMC",
            "Honda",
            "Hummer",
            "Hyundai",
            "Infiniti",
            "Isuzu",
            "Jaguar",
            "Jeep",
            "Kia",
            "Lada",
            "Lamborghini",
            "Lancia",
            "Land Rover",
            "Lexus",
            "Lincoln",
            "Mahindra",
            "Maserati",
            "Mazda",
            "Mercedes Benz",
            "Mini",
            "MG",
            "Mitsubishi",
            "Moszkvics",
            "Nissan",
            "NSU",
            "Opel",
            "Peugeot",
            "Piaggio",
            "Polonez",
            "Polski Fiat",
            "Pontiac",
            "Porsche",
            "Proton",
            "Renault",
            "Rolls Royce",
            "Rover",
            "Saab",
            "Seat",
            "Shuanghuan",
            "Smart",
            "SsangYong",
            "Subaru",
            "Suzuki",
            "Škoda",
            "Talbot",
            "Tata",
            "Tavria",
            "Toyota",
            "Trabant",
            "UAZ",
            "Vauxhall",
            "Volkswagen",
            "Volvo",
            "Wartburg",
            "Zastava",
            "Ostalo"});
            this.cbMarka.Location = new System.Drawing.Point(85, 57);
            this.cbMarka.Name = "cbMarka";
            this.cbMarka.Size = new System.Drawing.Size(347, 21);
            this.cbMarka.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Marka:";
            // 
            // txId
            // 
            this.txId.Location = new System.Drawing.Point(85, 31);
            this.txId.Name = "txId";
            this.txId.ReadOnly = true;
            this.txId.Size = new System.Drawing.Size(347, 20);
            this.txId.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Vozila:";
            // 
            // cmdOtkazi
            // 
            this.cmdOtkazi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOtkazi.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdOtkazi.Location = new System.Drawing.Point(472, 301);
            this.cmdOtkazi.Name = "cmdOtkazi";
            this.cmdOtkazi.Size = new System.Drawing.Size(75, 23);
            this.cmdOtkazi.TabIndex = 6;
            this.cmdOtkazi.Text = "O&tkaži";
            this.cmdOtkazi.UseVisualStyleBackColor = true;
            this.cmdOtkazi.Click += new System.EventHandler(this.cmdOtkazi_Click);
            // 
            // cmdOk
            // 
            this.cmdOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOk.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdOk.Location = new System.Drawing.Point(472, 272);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(75, 23);
            this.cmdOk.TabIndex = 5;
            this.cmdOk.Text = "&Ok";
            this.cmdOk.UseVisualStyleBackColor = true;
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // frmAutomobil
            // 
            this.AcceptButton = this.cmdOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cmdOtkazi;
            this.ClientSize = new System.Drawing.Size(559, 336);
            this.Controls.Add(this.cmdOtkazi);
            this.Controls.Add(this.cmdOk);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "frmAutomobil";
            this.Text = "Automobil";
            this.Load += new System.EventHandler(this.frmAutomobil_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudVrata)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGodiste)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbMenjac;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbPogon;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbKubikaza;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nudGodiste;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbModel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbMarka;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudVrata;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbGorivo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbKaroserija;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button cmdOtkazi;
        private System.Windows.Forms.Button cmdOk;
    }
}