﻿namespace CarRent
{
    partial class frmRezervacija
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbModel = new System.Windows.Forms.ComboBox();
            this.cbGodiste = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbKubikaza = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbKaroserija = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbBrojVrata = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbMenjac = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbPogon = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbGorivo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnPrikaz = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnRezervisi = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbPonude = new System.Windows.Forms.ListBox();
            this.cbMarka = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.dtRezOd = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.dtRezDo = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.txCena = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbModel
            // 
            this.cbModel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbModel.FormattingEnabled = true;
            this.cbModel.Location = new System.Drawing.Point(3, 84);
            this.cbModel.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.cbModel.Name = "cbModel";
            this.cbModel.Size = new System.Drawing.Size(132, 21);
            this.cbModel.TabIndex = 3;
            this.cbModel.SelectedIndexChanged += new System.EventHandler(this.FilterConbo_SelectedIndexChanged);
            // 
            // cbGodiste
            // 
            this.cbGodiste.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbGodiste.FormattingEnabled = true;
            this.cbGodiste.Location = new System.Drawing.Point(3, 139);
            this.cbGodiste.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.cbGodiste.Name = "cbGodiste";
            this.cbGodiste.Size = new System.Drawing.Size(132, 21);
            this.cbGodiste.TabIndex = 5;
            this.cbGodiste.SelectedIndexChanged += new System.EventHandler(this.FilterConbo_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Godište:";
            // 
            // cbKubikaza
            // 
            this.cbKubikaza.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbKubikaza.FormattingEnabled = true;
            this.cbKubikaza.Location = new System.Drawing.Point(148, 29);
            this.cbKubikaza.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.cbKubikaza.Name = "cbKubikaza";
            this.cbKubikaza.Size = new System.Drawing.Size(132, 21);
            this.cbKubikaza.TabIndex = 7;
            this.cbKubikaza.SelectedIndexChanged += new System.EventHandler(this.FilterConbo_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(148, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Kubikaža:";
            // 
            // cbKaroserija
            // 
            this.cbKaroserija.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbKaroserija.FormattingEnabled = true;
            this.cbKaroserija.Location = new System.Drawing.Point(148, 84);
            this.cbKaroserija.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.cbKaroserija.Name = "cbKaroserija";
            this.cbKaroserija.Size = new System.Drawing.Size(132, 21);
            this.cbKaroserija.TabIndex = 9;
            this.cbKaroserija.SelectedIndexChanged += new System.EventHandler(this.FilterConbo_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(148, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Karoserija:";
            // 
            // cbBrojVrata
            // 
            this.cbBrojVrata.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbBrojVrata.FormattingEnabled = true;
            this.cbBrojVrata.Location = new System.Drawing.Point(148, 139);
            this.cbBrojVrata.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.cbBrojVrata.Name = "cbBrojVrata";
            this.cbBrojVrata.Size = new System.Drawing.Size(132, 21);
            this.cbBrojVrata.TabIndex = 11;
            this.cbBrojVrata.SelectedIndexChanged += new System.EventHandler(this.FilterConbo_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(148, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Broj vrata:";
            // 
            // cbMenjac
            // 
            this.cbMenjac.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMenjac.FormattingEnabled = true;
            this.cbMenjac.Location = new System.Drawing.Point(293, 139);
            this.cbMenjac.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.cbMenjac.Name = "cbMenjac";
            this.cbMenjac.Size = new System.Drawing.Size(134, 21);
            this.cbMenjac.TabIndex = 17;
            this.cbMenjac.SelectedIndexChanged += new System.EventHandler(this.FilterConbo_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(293, 116);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Menjač:";
            // 
            // cbPogon
            // 
            this.cbPogon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPogon.FormattingEnabled = true;
            this.cbPogon.Location = new System.Drawing.Point(293, 84);
            this.cbPogon.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.cbPogon.Name = "cbPogon";
            this.cbPogon.Size = new System.Drawing.Size(134, 21);
            this.cbPogon.TabIndex = 15;
            this.cbPogon.SelectedIndexChanged += new System.EventHandler(this.FilterConbo_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(293, 61);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Pogon:";
            // 
            // cbGorivo
            // 
            this.cbGorivo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbGorivo.FormattingEnabled = true;
            this.cbGorivo.Location = new System.Drawing.Point(293, 29);
            this.cbGorivo.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.cbGorivo.Name = "cbGorivo";
            this.cbGorivo.Size = new System.Drawing.Size(134, 21);
            this.cbGorivo.TabIndex = 13;
            this.cbGorivo.SelectedIndexChanged += new System.EventHandler(this.FilterConbo_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(293, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Gorivo:";
            // 
            // btnPrikaz
            // 
            this.btnPrikaz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.btnPrikaz, 3);
            this.btnPrikaz.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPrikaz.Location = new System.Drawing.Point(3, 168);
            this.btnPrikaz.Name = "btnPrikaz";
            this.btnPrikaz.Size = new System.Drawing.Size(431, 44);
            this.btnPrikaz.TabIndex = 18;
            this.btnPrikaz.Text = "Prikaži dostupne termine odabranog automobila";
            this.btnPrikaz.UseVisualStyleBackColor = true;
            this.btnPrikaz.Click += new System.EventHandler(this.btnPrikaz_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.Controls.Add(this.btnRezervisi, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbPonude, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.btnPrikaz, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.cbMarka, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.cbMenjac, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label7, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.cbModel, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.cbPogon, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label8, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.cbGodiste, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.cbGorivo, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label9, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbKubikaza, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.cbBrojVrata, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label5, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label6, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.cbKaroserija, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.dtRezOd, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.dtRezDo, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.label12, 2, 10);
            this.tableLayoutPanel1.Controls.Add(this.txCena, 2, 11);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 13;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(437, 487);
            this.tableLayoutPanel1.TabIndex = 19;
            // 
            // btnRezervisi
            // 
            this.btnRezervisi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.btnRezervisi, 3);
            this.btnRezervisi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnRezervisi.Location = new System.Drawing.Point(3, 440);
            this.btnRezervisi.Name = "btnRezervisi";
            this.btnRezervisi.Size = new System.Drawing.Size(431, 44);
            this.btnRezervisi.TabIndex = 26;
            this.btnRezervisi.Text = "Rezerviši";
            this.btnRezervisi.UseVisualStyleBackColor = true;
            this.btnRezervisi.Click += new System.EventHandler(this.btnRezervisi_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Marka:";
            // 
            // lbPonude
            // 
            this.lbPonude.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.lbPonude, 3);
            this.lbPonude.FormattingEnabled = true;
            this.lbPonude.Location = new System.Drawing.Point(3, 225);
            this.lbPonude.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.lbPonude.Name = "lbPonude";
            this.lbPonude.Size = new System.Drawing.Size(431, 82);
            this.lbPonude.TabIndex = 19;
            this.lbPonude.SelectedIndexChanged += new System.EventHandler(this.lbPonude_SelectedIndexChanged);
            // 
            // cbMarka
            // 
            this.cbMarka.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMarka.FormattingEnabled = true;
            this.cbMarka.Location = new System.Drawing.Point(3, 29);
            this.cbMarka.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.cbMarka.Name = "cbMarka";
            this.cbMarka.Size = new System.Drawing.Size(132, 21);
            this.cbMarka.TabIndex = 3;
            this.cbMarka.SelectedIndexChanged += new System.EventHandler(this.FilterConbo_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Model:";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 327);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 25);
            this.label10.TabIndex = 20;
            this.label10.Text = "Odaberite datum preuzimanja:";
            // 
            // dtRezOd
            // 
            this.dtRezOd.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tableLayoutPanel1.SetColumnSpan(this.dtRezOd, 2);
            this.dtRezOd.Location = new System.Drawing.Point(3, 357);
            this.dtRezOd.Name = "dtRezOd";
            this.dtRezOd.Size = new System.Drawing.Size(225, 20);
            this.dtRezOd.TabIndex = 21;
            this.dtRezOd.ValueChanged += new System.EventHandler(this.datum_ValueChanged);
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 388);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(132, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Odaberite datum vraćanja:";
            // 
            // dtRezDo
            // 
            this.dtRezDo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tableLayoutPanel1.SetColumnSpan(this.dtRezDo, 2);
            this.dtRezDo.Location = new System.Drawing.Point(3, 412);
            this.dtRezDo.Name = "dtRezDo";
            this.dtRezDo.Size = new System.Drawing.Size(225, 20);
            this.dtRezDo.TabIndex = 23;
            this.dtRezDo.ValueChanged += new System.EventHandler(this.datum_ValueChanged);
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(293, 388);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(129, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Ukupna cena rezervacije:";
            // 
            // txCena
            // 
            this.txCena.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txCena.Enabled = false;
            this.txCena.Location = new System.Drawing.Point(293, 412);
            this.txCena.Name = "txCena";
            this.txCena.Size = new System.Drawing.Size(129, 20);
            this.txCena.TabIndex = 25;
            // 
            // frmRezervacija
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 511);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(480, 550);
            this.Name = "frmRezervacija";
            this.Text = "Rezervacija";
            this.Load += new System.EventHandler(this.frmRezervacija_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbModel;
        private System.Windows.Forms.ComboBox cbGodiste;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbKubikaza;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbKaroserija;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbBrojVrata;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbMenjac;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbPogon;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbGorivo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnPrikaz;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbMarka;
        private System.Windows.Forms.Button btnRezervisi;
        private System.Windows.Forms.ListBox lbPonude;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dtRezOd;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtRezDo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txCena;
    }
}