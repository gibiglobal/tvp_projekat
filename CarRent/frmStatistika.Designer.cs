﻿namespace CarRent
{
    partial class frmStatistika
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dtMesec = new System.Windows.Forms.DateTimePicker();
            this.cmdPrikaz = new System.Windows.Forms.Button();
            this.lbRezervacije = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Izbor meseca:";
            // 
            // dtMesec
            // 
            this.dtMesec.Location = new System.Drawing.Point(91, 21);
            this.dtMesec.Name = "dtMesec";
            this.dtMesec.Size = new System.Drawing.Size(200, 20);
            this.dtMesec.TabIndex = 1;
            // 
            // cmdPrikaz
            // 
            this.cmdPrikaz.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cmdPrikaz.Location = new System.Drawing.Point(312, 17);
            this.cmdPrikaz.Name = "cmdPrikaz";
            this.cmdPrikaz.Size = new System.Drawing.Size(160, 29);
            this.cmdPrikaz.TabIndex = 2;
            this.cmdPrikaz.Text = "Prikaži statistiku";
            this.cmdPrikaz.UseVisualStyleBackColor = true;
            this.cmdPrikaz.Click += new System.EventHandler(this.cmdPrikaz_Click);
            // 
            // lbRezervacije
            // 
            this.lbRezervacije.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbRezervacije.FormattingEnabled = true;
            this.lbRezervacije.Location = new System.Drawing.Point(12, 304);
            this.lbRezervacije.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.lbRezervacije.Name = "lbRezervacije";
            this.lbRezervacije.Size = new System.Drawing.Size(776, 134);
            this.lbRezervacije.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "label2";
            // 
            // frmStatistika
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbRezervacije);
            this.Controls.Add(this.cmdPrikaz);
            this.Controls.Add(this.dtMesec);
            this.Controls.Add(this.label1);
            this.Name = "frmStatistika";
            this.Text = "Statistika";
            this.Load += new System.EventHandler(this.frmStatistika_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmStatistika_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtMesec;
        private System.Windows.Forms.Button cmdPrikaz;
        private System.Windows.Forms.ListBox lbRezervacije;
        private System.Windows.Forms.Label label2;
    }
}