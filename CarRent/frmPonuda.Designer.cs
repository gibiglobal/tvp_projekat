﻿namespace CarRent
{
    partial class frmPonuda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbAutomobil = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.nudCena = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.dtDatumDo = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtDatumOd = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.cmdOtkazi = new System.Windows.Forms.Button();
            this.cmdOk = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCena)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbAutomobil);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(397, 74);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Automobil";
            // 
            // cbAutomobil
            // 
            this.cbAutomobil.FormattingEnabled = true;
            this.cbAutomobil.Location = new System.Drawing.Point(110, 31);
            this.cbAutomobil.Name = "cbAutomobil";
            this.cbAutomobil.Size = new System.Drawing.Size(271, 21);
            this.cbAutomobil.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Izaberite automobil:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.nudCena);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.dtDatumDo);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.dtDatumOd);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(12, 103);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(397, 92);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Podaci o ponudi";
            // 
            // nudCena
            // 
            this.nudCena.DecimalPlaces = 2;
            this.nudCena.Location = new System.Drawing.Point(89, 56);
            this.nudCena.Name = "nudCena";
            this.nudCena.Size = new System.Drawing.Size(120, 20);
            this.nudCena.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Cena po danu:";
            // 
            // dtDatumDo
            // 
            this.dtDatumDo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDatumDo.Location = new System.Drawing.Point(258, 30);
            this.dtDatumDo.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtDatumDo.Name = "dtDatumDo";
            this.dtDatumDo.Size = new System.Drawing.Size(123, 20);
            this.dtDatumDo.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(230, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "do:";
            // 
            // dtDatumOd
            // 
            this.dtDatumOd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDatumOd.Location = new System.Drawing.Point(89, 30);
            this.dtDatumOd.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtDatumOd.Name = "dtDatumOd";
            this.dtDatumOd.Size = new System.Drawing.Size(123, 20);
            this.dtDatumOd.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Raspoloživ od:";
            // 
            // cmdOtkazi
            // 
            this.cmdOtkazi.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdOtkazi.Location = new System.Drawing.Point(239, 219);
            this.cmdOtkazi.Name = "cmdOtkazi";
            this.cmdOtkazi.Size = new System.Drawing.Size(75, 23);
            this.cmdOtkazi.TabIndex = 8;
            this.cmdOtkazi.Text = "O&tkaži";
            this.cmdOtkazi.UseVisualStyleBackColor = true;
            this.cmdOtkazi.Click += new System.EventHandler(this.cmdOtkazi_Click);
            // 
            // cmdOk
            // 
            this.cmdOk.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdOk.Location = new System.Drawing.Point(116, 219);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(75, 23);
            this.cmdOk.TabIndex = 7;
            this.cmdOk.Text = "&Ok";
            this.cmdOk.UseVisualStyleBackColor = true;
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // frmPonuda
            // 
            this.AcceptButton = this.cmdOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cmdOtkazi;
            this.ClientSize = new System.Drawing.Size(424, 256);
            this.Controls.Add(this.cmdOtkazi);
            this.Controls.Add(this.cmdOk);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmPonuda";
            this.Text = "Ponuda";
            this.Load += new System.EventHandler(this.frmPonuda_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCena)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbAutomobil;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudCena;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtDatumDo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtDatumOd;
        private System.Windows.Forms.Button cmdOtkazi;
        private System.Windows.Forms.Button cmdOk;
    }
}