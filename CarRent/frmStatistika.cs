﻿using CarRent.Modeli;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRent
{
    public partial class frmStatistika : Form
    {
        private bool iscrtano;
        private List<Rezervacija> rezervacije;
        

        public frmStatistika()
        {
            InitializeComponent();

            dtMesec.Format = DateTimePickerFormat.Custom;
            dtMesec.CustomFormat = "MM. yyyy";
            dtMesec.ShowUpDown = true;
        }

        private void frmStatistika_Load(object sender, EventArgs e)
        {

        }

        private void UcitajPodatke()
        {
            var parentForm = (frmMain)this.MdiParent;

            if (parentForm == null)
            {
                MessageBox.Show("Forma nema dostupa do podataka!");
                return;
            }

            rezervacije = parentForm.Rezervacije
                .FindAll(f => f.DanaUMesecu(dtMesec.Value) > 0);
        }

        private void CrtajPieChart(object sender, PaintEventArgs e)
        {
            if (rezervacije == null) return;

            int availHeight = lbRezervacije.Top - 55;
            int availWidth = this.Width - 24;

            var chartSize = Math.Min(availHeight - 20, availWidth - 20);
            var x = (availWidth - chartSize) / 2;
            var y = 55 + (availHeight - chartSize) / 2;

            float start = -90F;
            float sweep = 0;
            Rectangle r = new Rectangle(x, y, chartSize, chartSize);
            e.Graphics.DrawEllipse(Pens.Black, r);
            e.Graphics.FillEllipse(Brushes.Blue, r);

            var danaUMesecu = DateTime.DaysInMonth(dtMesec.Value.Year, dtMesec.Value.Month);
            var rnd = new Random();
            lbRezervacije.Items.Clear();
            foreach (var rez in rezervacije)
            {
                var rezUMesecu = rez.DanaUMesecu(dtMesec.Value);
                sweep = rezUMesecu * 360f / danaUMesecu;
                lbRezervacije.Items.Add(string.Format("Automobil ID: {0}   Kupac ID: {1}   REZERVISAN U MESECU: {2}", rez.AutomobilId, rez.KupacId, rezUMesecu));
                var color = new SolidBrush(Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256)));
                e.Graphics.FillPie(color, r, start, sweep);
                start += sweep;
            }
        }

        private void cmdPrikaz_Click(object sender, EventArgs e)
        {
            UcitajPodatke();
            this.Paint += CrtajPieChart;
            this.Invalidate();
        }

        private void frmStatistika_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
