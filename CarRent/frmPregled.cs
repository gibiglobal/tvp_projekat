﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace CarRent
{
    public partial class frmPregled<T> : Form
    {
        private IList<T> objekti;
        private T izabraniObjekat;

        // Delegati
        public delegate void DodajMethod();
        public delegate void PromeniMethod(T objekat);
        public delegate void ObrisiMethod(T objekat);

        // Metode
        public DodajMethod OnDodaj;
        public PromeniMethod OnPromeni;
        public ObrisiMethod OnObrisi;

        public frmPregled(IList<T> objekti)
        {
            InitializeComponent();
            this.objekti = objekti;
        }

        private void frmPregled_Load(object sender, EventArgs e)
        {
            PopulisiListu();
            this.Text = lbNaslov.Text;
        }

        private void PopulisiListu()
        {
            foreach (var item in objekti)
            {
                lbLista.Items.Add(item.ToString());
            }
        }

        private void lbLista_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbLista.SelectedIndex >= 0)
                izabraniObjekat = objekti[lbLista.SelectedIndex];   
        }

        private void cmdDodaj_Click(object sender, EventArgs e)
        {
            if (OnDodaj == null) return;

            OnDodaj();
        }

        private void cmdPromeni_Click(object sender, EventArgs e)
        {
            if (OnPromeni == null) return;

            if (izabraniObjekat != null)
            {
                OnPromeni(izabraniObjekat);
            }
            else
            {
                MessageBox.Show("Nije izabrana ni jedna stavka iz liste! Izaberite stavku i pokušajte ponovo.");
            }
        }

        private void cmdBriši_Click(object sender, EventArgs e)
        {
            if (OnObrisi == null) return;

            if (izabraniObjekat != null)
            {
                if (MessageBox.Show("Da li zaista želite da izbrišete izabranu stavku?", "Brisanje stavke", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    OnObrisi(izabraniObjekat);
            }
            else
            {
                MessageBox.Show("Nije izabrana ni jedna stavka iz liste! Izaberite stavku i pokušajte ponovo.");
            }
        }

        private void cmdIzlaz_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lbLista_DoubleClick(object sender, EventArgs e)
        {
            if (lbLista.SelectedIndex >= 0)
            {
                izabraniObjekat = objekti[lbLista.SelectedIndex];
            }

            cmdPromeni_Click(this.cmdPromeni, new EventArgs());
        }
    }
}
